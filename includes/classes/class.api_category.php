<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.api_client.php');


/**
 * Used for modular implementation, class to encapsulate the corresponding API functionality
 *
 */
class api_category extends api_client
{
	var $aCategoryIds= 		array(0); 	// the ids of categories to be returned



	/**
	 * Gets an array of merchant(s)
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 	array	 $oParams->aCategoryIds
	 * @return 	array	 of categories
	 */
	function getCategory($oParams)
	{
		// sanity checks
		if (is_array($oParams->aCategoryIds)) {
			$this->aCategoryIds= array_unique($oParams->aCategoryIds);
		}
		else {
			return false;
		}

        $aParams= array('aCategoryIds' => $this->aCategoryIds);


       	// make the SOAP call
        $this->call('getCategory', $aParams);


        $aCategories= array();

        // re-assign to be keyed by id
        foreach ($this->oResponse->getCategoryReturn as $oCategory) {
 			$aCategories[$oCategory->iId]= $oCategory;
        }


        return $aCategories;
	}

}


?>