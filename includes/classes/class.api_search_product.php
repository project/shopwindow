<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.api_client.php');


/**
 * Used for modular implementation, class to encapsulate the corresponding API functionality
 *
 */
class api_search_product extends api_client
{
	var $sQuery= 				'';			// the query to search for
	var $sMode= 				'all';		// search modifier, one of: [phrase, boolean, all, any]
	var $sSort=					'relevance';// one of [relevance, new, old]
	var $iCategoryId=			0;			// the category to which the result should be restricted
	var $aMerchantIds= 			array(); 	// the merchants to which the result should be restricted
	var $iLimit=				10;			// the maximum number of products
	var $iOffset=				0;			// the offset on the list
	var $bIncludeDescendants=	true; 	 	// show products from within children categories




	/**
	 * Gets the list of products that match the passed keyword
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @param 	string 	 $oParams->sKeyword
	 * @param 	string 	 $oParams->sMode
	 * @param 	string 	 $oParams->sSort
	 * @param 	int 	 $oParams->iCategoryId
	 * @param 	array	 $oParams->aMerchantIds
	 * @param 	int		 $oParams->iLimit
	 * @param 	int		 $oParams->iOffset
	 * @param 	bool	 $oParams->bIncludeDescendants
	 * @return 	array 	 of products
	 */
	function searchProduct($oParams)
	{
		// sanity checks
		if (!empty($oParams->sQuery)) {
			$this->sQuery= $oParams->sQuery;
		}
		else {
			return false;
		}


		$this->sMode=			 !empty($oParams->sMode)			 ? $oParams->sMode			  : $this->sMode;
		$this->sSort=			 !empty($oParams->sSort)			 ? $oParams->sSort			  : $this->sSort;
		$this->iCategoryId=		 is_numeric($oParams->iCategoryId)	 ? $oParams->iCategoryId	  : $this->iCategoryId;
		$this->aMerchantIds=	 is_array($oParams->aMerchantIds)	 ? $oParams->aMerchantIds	  : $this->aMerchantIds;
		$this->iLimit=			 is_numeric($oParams->iLimit)		 ? $oParams->iLimit			  : $this->iLimit;
		$this->iOffset= 		 is_numeric($oParams->iOffset)		 ? $oParams->iOffset		  : $this->iOffset;
		$this->bIncludeDescendants= is_bool($oParams->bIncludeDescendants) ? $oParams->bIncludeDescendants : $this->bIncludeDescendants;


        $aParams= array('sQuery' => $this->sQuery,
        				'sMode' => $this->sMode,
        				'sSort' => $this->sSort,
        				'iCategoryId' => $this->iCategoryId,
        				'aMerchantIds' => $this->aMerchantIds,
        				'iLimit' => $this->iLimit,
        				'iOffset' => $this->iOffset,
        				'bIncludeDescendants' =>$this->bIncludeDescendants);


       	// make the SOAP call
//				$a=time();
				$this->call('searchProduct', $aParams);
	//			$b=time()=$a;
		//		echo $b." seconds";
        $aProducts= $this->oResponse->searchProductReturn;


        return $aProducts;
	}

}


?>