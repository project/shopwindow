<?php
/**
*
* ShopWindow Toolset
*
* Copyright (C) 2007 Digital Window Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.shopcore.php');
require_once('class.api_client.php');

class pageProduct extends shopcore
{
    // required data items
    var $aRequired = array( 'iProductId' );



    /**
    * Constructor
    *
    * @access    public
    * @copyright affiliatewindow
    * @author    PAI <paul@affiliatewindow.com>
    */
    function pageProduct()
    {
        $this->setPageHtml();
    }



    /**
    * get page data requested
    *
    * @access    public
    * @copyright affiliatewindow
    * @author    PAI <paul@affiliatewindow.com>
    */
    function buildData()
    {
        # check requirements
        if ( !is_numeric($this->iProductId) ) return false;

        $oClient = new api_client();


        # product
        $aProductParams = array('aProductIds'=> array($this->iProductId) );
        // SOAP
        $oProduct = $oClient->call('getProduct', $aProductParams);

        $this->oProduct = $oProduct->getProductReturn[0];


        if ( is_numeric($this->oProduct->iCategoryId) )
        {
            $aCategoryParams = array( 'aCategoryIds' => array($this->oProduct->iCategoryId));
            // SOAP
            $oCategory = $oClient->call('getCategory', $aCategoryParams);

            $this->oCategory = $oCategory->getCategoryReturn[0];

            # category tree
	        $aCategoryTreeParams = array("iCategoryId"=>$this->oProduct->iCategoryId, "bIncludeCounts"=>TREE_COUNTS);
	        // SOAP
	        $this->oCategoryList = $oClient->call('getCategoryTree', $aCategoryTreeParams);
        }


        # merchant details (product owner)
        if ( !is_numeric($this->iMerchantId) ) $this->iMerchantId = $this->oProduct->iMerchantId;

        if ( is_numeric($this->iMerchantId) )
        {
            $aMerchantParams = array('aMerchantIds'=> array($this->iMerchantId) );
            // SOAP
        	$oMerchant = $oClient->call('getMerchant', $aMerchantParams);

            $this->oProduct->oMerchant = $oMerchant->getMerchantReturn[0];
        }

        # category bredcrumb
        /*if ( $this->iCategoryId>0 )
        {
            $aCategoryParams = array('aCategoryIds'=> array($this->iCategoryId) );
            // SOAP
            $oCategory = $oClient->call('getCategory', $aCategoryParams);

            $this->oCategory = $oCategory->getCategoryReturn[0];

            // Acquire the parent
            if ( $this->oCategory->iParentId > 0 )
            {
                $this->oCategoryPath = $oClient->call('getCategoryPath', array('iCategoryId' => $this->iCategoryId) );
            }
        }*/

        # category bredcrumb
        if ( $this->oProduct->iCategoryId>0 ) {
            // Acquire the parent
            if ( $this->oCategory->iParentId > 0 ) {
                $this->oCategoryPath = $oClient->call('getCategoryPath', array('iCategoryId' => $this->oProduct->iCategoryId) );
            }
        }

    }

}
# end of class

?>
