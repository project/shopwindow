<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once(HOME_PATH.'global.inc.php');
require_once('class.shopcore.php');

require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'smarty'.DIRECTORY_SEPARATOR.'libs'.DIRECTORY_SEPARATOR.'Smarty.class.php');


class Smarty_SW extends Smarty
{

	function Smarty_SW()
	{
		parent::Smarty();

		// set paths
		$this->compile_dir=  HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'smarty'.DIRECTORY_SEPARATOR.'templates_c'.DIRECTORY_SEPARATOR;
		$this->config_dir= 	 HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'smarty'.DIRECTORY_SEPARATOR.'configs'.DIRECTORY_SEPARATOR;
		$this->cache_dir= 	 HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'smarty'.DIRECTORY_SEPARATOR.'cache'.DIRECTORY_SEPARATOR;

		// load SMARTY config file
		$this->config_load('config.inc');

		// get the template Dir
		$sTemplateDir= $this->get_config_vars('templateDir');
		$sTemplateUrl= base_path().drupal_get_path('module', 'shopwindow') .'/templates'.DIRECTORY_SEPARATOR.$sTemplateDir.DIRECTORY_SEPARATOR;

		// specify the directory using the var from config
		$this->template_dir= HOME_PATH.'templates'.DIRECTORY_SEPARATOR.$sTemplateDir.DIRECTORY_SEPARATOR;

		// set global vars
		$this->assign('DIRECTORY_SEPARATOR', DIRECTORY_SEPARATOR);
		$this->assign('P_INDEX', T_INDEX);
		$this->assign('P_SEARCH', T_SEARCH);
		$this->assign('P_CATEGORY', T_CATEGORY);

		$this->assign('noImage', $sTemplateUrl.'images'.DIRECTORY_SEPARATOR.'noimage.gif');
		$this->assign('noLogo', $sTemplateUrl.'images'.DIRECTORY_SEPARATOR.'nologo.gif');
		$this->assign('templatePath', $sTemplateUrl );


		// force compile on every load, disable once live
		$this->force_compile= true;
	}

}



?>