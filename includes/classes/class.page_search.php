<?php
/**
*
* ShopWindow Toolset
*
* Copyright (C) 2007 Digital Window Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.shopcore.php');
require_once('class.api_client.php');


class pageSearch extends shopcore
{
    var $sSearchSort;
    var $sSearchMode;
    var $iSearchLimit;
    var $iSearchOffset;
    var $bIncludeDescendants;
    var $iFeaturedMerchantsLimit= 6;



    /**
    * Constructor
    *
    * @access    public
    * @copyright affiliatewindow
    * @author    PAI <paul@affiliatewindow.com>
    */
    function pageSearch($iSearchLimit=10, $iSearchOffset=0, $sSearchMode='all', $sSearchSort='relevance', $bIncludeDescendants=true)
    {
        if ( strlen($sSearchSort)>0 ) $this->sSearchSort = $sSearchSort; else $this->sSearchSort='relevance';
        if ( strlen($sSearchMode)>0 ) $this->sSearchMode = $sSearchMode; else $this->sSearchMode='all';
        if ( is_numeric($iSearchLimit) ) $this->iSearchLimit = $iSearchLimit; else $this->iSearchLimit=10;
        if ( is_numeric($iSearchOffset) ) $this->iSearchOffset = $iSearchOffset; else $this->iSearchOffset=0;
        if ( $bIncludeDescendants==true ) $this->bIncludeDescendants = true; else $this->bIncludeDescendants=false;

        $this->setPageHtml();
    }



    /**
    * get page data requested
    *
    * @access    public
    * @copyright affiliatewindow
    * @author    PAI <paul@affiliatewindow.com>
    */
    function buildData()
    {
        # check requirements
        if ( strlen($this->sKeyword)==0 ) return false;


        $oClient = new api_client();

        // force zero when empty to comply with api
		if (empty($this->iCategoryId))
		{
			$this->iCategoryId = "0";
		}

        # for the search box
        $aCategoryTreeParams = array("iCategoryId"=>$this->iCategoryId, "bIncludeCounts"=>TREE_COUNTS);
        // SOAP
        $this->oCategoryList = $oClient->call('getCategoryTree', $aCategoryTreeParams);


        $aMerchantIds = array();
        if ( is_numeric($this->iMerchantId) ) $aMerchantIds[] = $this->iMerchantId;

        # keyword search for suggested categories
        $aSearchParams = array('sQuery'=>$this->sKeyword, 'aMerchantIds' => $aMerchantIds );
        // SOAP
        $this->oCatSearch = $oClient->call('getSuggestedCategory', $aSearchParams);

        # keyword search for products
        $aSearchParams = array('sQuery'=>$this->sKeyword, 'sMode'=>$this->sSearchMode, 'sSort'=>$this->sSearchSort, 'iOffset'=>$this->iSearchOffset, 'iLimit'=>$this->iSearchLimit, 'bIncludeDescendants'=>$this->bIncludeDescendants, 'aMerchantIds' => $aMerchantIds, 'iCategoryId' => $this->iCategoryId );

        // SOAP
    	$this->oSearch = $oClient->call('searchProduct', $aSearchParams);

        # get the underlying details for products
        if ( count($this->oSearch->searchProductReturn)>0 )
        {
            $aMerchantParams = array();
            $aCategoryParams = array();
            foreach ( $this->oSearch->searchProductReturn as $oProd)
            {
                $aMerchantParams[] = $oProd->iMerchantId;
		$aCategoryParams[] = $oProd->iCategoryId;
            }

            # get the merchant objs for these products
            $aMerchs = array();
            $aMerchantParams = array('aMerchantIds'=> array_unique($aMerchantParams) );

            // SOAP
        	$oMerchants = $oClient->call('getMerchant', $aMerchantParams);

            foreach($oMerchants->getMerchantReturn as $oMerchant)
            {
                $this->aMerchs[$oMerchant->iId] = $oMerchant;
            }

            # get the category objs for these products
            $aCats = array();
            $aCategoryParams = array('aCategoryIds'=>array_unique($aCategoryParams));
            // SOAP
            $oCategories = $oClient->call('getCategory', $aCategoryParams);

            foreach($oCategories->getCategoryReturn as $oCategory)
            {
                $this->aCats[$oCategory->iId] = $oCategory;
            }
        }

        # hotpicks
        $aHotPickParams = array("iLimit"=>3);
        // SOAP
    	$this->oHotPick = $oClient->call('getHotPickProductList', $aHotPickParams);

        # category bredcrumb
        if ( $this->iCategoryId>0 )
        {
            $aCategoryParams = array('aCategoryIds'=>array($this->iCategoryId) );
            // SOAP
        	$oCategory = $oClient->call('getCategory', $aCategoryParams);

            $this->oCategory = $oCategory->getCategoryReturn[0];

            // Acquire the breadcrumb
            if ( $this->oCategory->iParentId > 0 )
            {
            	$this->oCategoryPath = $oClient->call('getCategoryPath', array('iCategoryId' => $this->iCategoryId) );
            }

        }

		# featured merchants
        $aFeatureMerchantParams = array('iCategoryId' => $this->iCategoryId, 'iLimit' => $this->iFeaturedMerchantsLimit);
        // SOAP
        $this->oFeaturedMerchants = $oClient->call('getFeaturedMerchants', $aFeatureMerchantParams);

    }

}
# end of class

?>
