<?php
/**
*
* ShopWindow Toolset
*
* Copyright (C) 2007 Digital Window Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.shopcore.php');
require_once('class.api_client.php');


class pageCategory extends shopcore
{
    var $sProdSort;
    var $iProdLimit;
    var $iProdOffset;
    var $bProdSubCats;
    var $iFeatureLimit;
    var $bTreeEmptyCats;
    var $bTreeEmptySubCats;
    var $aRefByDefinitions;



    /**
    * Constructor
    *
    * @access    public
    * @copyright affiliatewindow
    * @author    PAI <paul@affiliatewindow.com>
    */
    function pageCategory($sProdSort, $iProdLimit=10, $iProdOffset=0, $bProdSubCats=true, $iFeatureLimit=6, $bTreeEmptyCats=false, $bTreeEmptySubCats=false)
    {
        # setup the page objects
        if ( strlen($sProdSort)>0 ) $this->sProdSort = $sProdSort; else $this->sProdSort='popular';
        if ( is_numeric($iProdLimit) ) $this->iProdLimit = $iProdLimit; else $this->iProdLimit=10;
        if ( is_numeric($iProdOffset) ) $this->iProdOffset = $iProdOffset; else $this->iProdOffset=0;
        if ( is_numeric($iFeatureLimit) ) $this->iFeatureLimit = $iFeatureLimit; else $this->iFeatureLimit=8;
        if ( $bProdSubCats==true ) $this->bProdSubCats = true; else $this->bProdSubCats=false;
        if ( $bTreeEmptyCats==true ) $this->bTreeEmptyCats = true; else $this->bTreeEmptyCats=false;
        if ( $bTreeEmptySubCats==true ) $this->bTreeEmptySubCats = true; else $this->bTreeEmptySubCats=false;

        $this->setPageHtml();
    }



    /**
    * get page data requested
    *
    * @access    public
    * @copyright affiliatewindow
    * @author    PAI <paul@affiliatewindow.com>
    */
    function buildData()
    {
        $oClient = new api_client();


        if (!is_numeric($this->iCategoryId))
        {
        	$this->iCategoryId=0;
        }

        $aMerchantIds = array();

        # merchant
        if ( is_numeric($this->iMerchantId) )
        {
            $aMerchantIds[] = $this->iMerchantId;


            // SOAP
            $oMerchant = $oClient->call('getMerchant', array('aMerchantIds'=> $aMerchantIds ));

            $this->oMerchant = $oMerchant->getMerchantReturn[0];
        }

        # category
        if ( $this->iCategoryId > 0 )
        {
            $aCategoryParams = array('aCategoryIds'=>array($this->iCategoryId));

            // SOAP
            $oCategory = $oClient->call('getCategory', $aCategoryParams);

            $this->oCategory = $oCategory->getCategoryReturn[0];

            // Acquire full breadcrumb
            if ( $this->oCategory->iParentId > 0 )
            {
                // SOAP
                $this->oCategoryPath = $oClient->call('getCategoryPath', array('iCategoryId' => $this->iCategoryId) );
            }
        }


        # category tree
        $aCategoryTreeParams = array("iCategoryId"=>$this->iCategoryId, "aMerchantIds"=>$aMerchantIds, "bIncludeEmptyCats"=>$this->bTreeEmptyCats, "bIncludeCounts"=>TREE_COUNTS);
        // SOAP
        $this->oCategoryList = $oClient->call('getCategoryTree', $aCategoryTreeParams);



        # feature merchants
        $aFeatureMerchantParams = array('iCategoryId' => $this->iCategoryId, 'iLimit' => $this->iFeatureLimit);
        // SOAP
       $this->oFeaturedMerchants = $oClient->call('getFeaturedMerchants', $aFeatureMerchantParams);

        # products in this category
        $aProductListParams = array('iCategoryId' => $this->iCategoryId, 'aMerchantIds' => $aMerchantIds, 'sRefineByPairs' => $this->getRefineByQuery(), 'sSort' => $this->sProdSort, 'iLimit' => $this->iProdLimit, 'iOffset' => $this->iProdOffset, 'bIncludeDescendants' =>$this->bProdSubCats);


        // SOAP
        $this->oProductList = $oClient->call('getProductList', $aProductListParams);

        # get the products underlying details
        if ( count($this->oProductList->getProductListReturn)>0 )
        {
            $aMerchantParams = array();
            $aCategoryParams = array();

            foreach ( $this->oProductList->getProductListReturn as $oProd)
            {
                $aMerchantParams[] = $oProd->iMerchantId;
                $aCategoryParams[] = $oProd->iCategoryId;
            }


            $aMerchs = array();

            $aMerchantParams = array('aMerchantIds'=> array_unique($aMerchantParams) );

            // SOAP
            $oMerchants = $oClient->call('getMerchant', $aMerchantParams);

            foreach($oMerchants->getMerchantReturn as $oMerchant)
            {
                $this->aMerchs[$oMerchant->iId] = $oMerchant;
            }

            $aCats = array();
            $aCategoryParams = array('aCategoryIds'=>array_unique($aCategoryParams));

            // SOAP
            $oCategories = $oClient->call('getCategory', $aCategoryParams);

            foreach($oCategories->getCategoryReturn as $oCategory)
            {
                $this->aCats[$oCategory->iId] = $oCategory;
            }


        }

			# RefineBy
            $aRefineByParams = array('iCategoryId'=> $this->iCategoryId);

            // SOAP
            $oCategoryRefineBy = $oClient->call('getCategoryRefineBy', $aRefineByParams);

            $this->aRefByDefinitions = $oCategoryRefineBy->getCategoryRefineByReturn;

    }



} // class


?>
