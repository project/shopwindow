<?php
/**
*
* ShopWindow Toolset
*
* Copyright (C) 2007 Digital Window Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once(HOME_PATH.'global.inc.php');



/**
 * Base class for shopwindow.com.  Handles database connectivity and core error handling.
 */
class shopcore
{
	# Attributes
	var $sHostname; // the hostname the code is excuted on
	var $sEnviroment; // the environment we are in: live, virtual, olddev/testsite
	var $iAffiliateId= USER_ID;
	var $iMerchantId;
	var $iProductId;
	var $iCategoryId;
	var $sKeyword;
	var $aBreadCrumb = array ();
	var $aHomeBreadCrumb = array ('sName' => 'Home', 'sUrl' => T_INDEX );
	var $aRefineByActiveVals= array();



	/*
	* run HTML page specific tasks
	*
	* @access    public
	* @copyright affiliatewindow
	* @author    PAI <paul@affiliatewindow.com>
	*/
	function setPageHtml()
	{
        $this->gatherPageData();
        $this->buildData();
        $this->buildBreadCrumb();
	}



	/**
	 * Build a URL address based on script running and available parameters
         *
	 * @access 	public
	 * @author	PAI <paul@affiliatewindow.com>
	 * @copyright   Affiliate Window Ltd
	 * @param       String [$sPage] Page to be built
	 * @param       String [$sDisplay] Link Display Text withing the a href
	 * @param       Interger [$iProductId] Product Id
	 * @param       Interger [$iMerchantId] Merchant Id
	 * @param       Interger [$iCategoryId] Category Id
	 * @param       Interger [$iAffiliateId] Affiliate Id
	 * @param       String	 [$sRefineByVals] a string with refine by pairs
	 * @return      String - URL
	 */
	function buildUrl($sPage, $sDisplay='', $iProductId='', $iMerchantId='', $iCategoryId='', $iAffiliateId='', $sRefineByVals='')
	{
		$sReturn = '';

		switch ($sPage)
		{
			case T_PRODUCT:
				$sReturn .= T_PRODUCT.'?p='.$iProductId;
				if(is_numeric($iMerchantId)) $sReturn .= '&m='.$iMerchantId;
				if(is_numeric($iCategoryId)) $sReturn .= '&c='.$iCategoryId;
			break;
			case T_GOTO:
				# set the affiliate ID
				$sReturn .= T_GOTO.'?p='.$iProductId.'&a='.USER_ID;
				if(is_numeric($iCategoryId)) $sReturn .= '&c='.$iCategoryId;
				if(is_numeric($iMerchantId)) $sReturn .= '&m='.$iMerchantId;
			break;
			case T_SEARCH:
				$sReturn .= T_SEARCH.'?q='.$sDisplay;
				if(is_numeric($iCategoryId)) $sReturn .= '&c='.$iCategoryId;
				if(is_numeric($iMerchantId)) $sReturn .= '&m='.$iMerchantId;
				if(strlen($sRefineByVals)>0) $sReturn .= '&rb='.$sRefineByVals;
			break;
			case T_CATEGORY:
				$sReturn .= T_CATEGORY.'?c='.$iCategoryId;
				if(is_numeric($iMerchantId)) $sReturn .= '&m='.$iMerchantId;
				if(strlen($sRefineByVals)>0) $sReturn .= '&rb='.$sRefineByVals;
			break;
			case T_INDEX:
				$sReturn .= T_INDEX;
			break;
			# catch all - if page looks like a url, use it else goto the index
			default:
				/*if ( shopcore::isValidUrl($sPage) )
				{
				    $sReturn .= $sPage;
				}
				else
				{
				    $sReturn .= T_INDEX.'?z=malformedInput';
				}*/

				$sReturn .= !empty($_SERVER['QUERY_STRING']) ? $sPage.'?'.$_SERVER['QUERY_STRING'] : $sPage;

		}


		return $sReturn;
	}



	/*
	* gather data passed in querystring
	*
	* @access    public
	* @copyright affiliatewindow
	* @author    PAI <paul@affiliatewindow.com>
	* @Return    Void
	*/
	function gatherPageData()
	{
		# category
		if ( $_GET['c'] and is_numeric($_GET['c']) ) $this->iCategoryId = $_GET['c'];
		if ( $_POST['c'] and is_numeric($_POST['c']) ) $this->iCategoryId = $_POST['c'];
		# merchant
		if ( $_GET['m'] and is_numeric($_GET['m']) ) $this->iMerchantId = $_GET['m'];
		if ( $_POST['m'] and is_numeric($_POST['m']) ) $this->iMerchantId = $_POST['m'];
		# product
		if ( $_GET['p'] and is_numeric($_GET['p']) ) $this->iProductId = $_GET['p'];
		if ( $_POST['p'] and is_numeric($_POST['p']) ) $this->iProductId = $_POST['p'];
		# keyword
		if ( $_GET['q'] and strlen($_GET['q'])>0 ) $this->sKeyword = stripslashes($_GET['q']);
		if ( $_POST['q'] and strlen($_POST['q'])>0 ) $this->sKeyword = stripslashes($_POST['q']);
		# refine by
		if ( $_GET['rb'] and strlen($_GET['rb'])>0 ) $this->setRefineByQuery($_GET['rb']);
		if ( $_POST['rb'] and strlen($_POST['rb'])>0 ) $this->setRefineByQuery($_POST['rb']);
	}



	/*
	* return true or false based on a valid URL
	*
	* @access    public
	* @copyright affiliatewindow
	* @author    http://uk2.php.net/manual/en/function.parse-url.php#55392 <jon _at_ gaarsmand _dot_ com>
	* @param     String [$uri] URL address to a product
	* @Return    Boolean
	*/
	function isValidUrl($uri)
	{
		if (preg_match('/^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}' .'((:[0-9]{1,5})?\/.*)?$/i' ,$uri))
		{
			return true;
		}
		else
		{
			return false;
		}
	}



	/*
	* get the breadcrum in html form
	*
	* @access    public
	* @copyright affiliatewindow
	* @author    PAI <paul@affiliatewindow.com>
	* @return    Mixed false if no crumbs, else string html'ed output of $this->aBreadCrumb
	*/
	function getBreadCrumb($bHtml=true)
	{
		# if there are no crumbs, the loaf is complete
		if ( !array_diff($this->aBreadCrumb, $this->aHomeBreadCrumb) )
		{
			return false;
		}

		$iCrumbCount=0;
		$sReturn= '';

		foreach ( $this->aBreadCrumb as $aCrumb )
		{
			if ( strlen($aCrumb['sUrl'])>0 && $bHtml===true ) {
				$sReturn .= '<a href="'.$aCrumb['sUrl'].'">';
			}

			if ( $iCrumbCount==0 && $bHtml===false ) {
				$sReturn .= '';
			}
			else {
				$sReturn .= strlen($aCrumb['sName'])>40 ? substr($aCrumb['sName'], 0, 37).'...' : $aCrumb['sName'];
			}

			if ( strlen($aCrumb['sUrl'])>0 && $bHtml===true ) {
				$sReturn .= '</a>';
			}

			if ( $bHtml===true ) {
				$sReturn .= "\n";
			}

			$iCrumbCount++;

			if ( count($this->aBreadCrumb)!=$iCrumbCount ) {
				$sReturn .= ' > ';
			}
		}

		return $sReturn;
	}



	/*
	* build array of the breakcrumb
	*
	* @access    public
	* @copyright affiliatewindow
	* @author    PAI <paul@affiliatewindow.com>
	* @return    Array containing breadcrumb array ( 'sName', 'sUrl')
	*/
	function buildBreadCrumb()
	{
		$this->aBreadCrumb[] = $this->aHomeBreadCrumb;

		# check the page user is on
		switch($_SERVER['SCRIPT_NAME'])
		{
			case T_SEARCH:
				$this->aBreadCrumb[] = array ('sName' => 'Search', 'sUrl' => '');
			break;
			case T_PRODUCT:
				$this->aBreadCrumb[] = array ('sName' => 'Product', 'sUrl' => '');
			break;
			case T_CATEGORY:
				$this->aBreadCrumb[] = array ('sName' => 'Category', 'sUrl' => '');
			break;
		}

		# check category
		if ( is_object($this->oCategory) )
		{
			if ( is_array($this->oCategoryPath->getCategoryPathReturn) )
			{
			    $aCategoryBreadCrumb = array_reverse($this->oCategoryPath->getCategoryPathReturn);
			    foreach ( $aCategoryBreadCrumb as $oBreadCrumb)
			    {
			        $this->aBreadCrumb[] = array ('sName' => $oBreadCrumb->sName, 'sUrl' => shopcore::buildUrl(T_CATEGORY, $oBreadCrumb->sName, '', '', $oBreadCrumb->iId, '', false) );
			    }
			}

			$this->aBreadCrumb[] = array ('sName' => $this->oCategory->sName, 'sUrl' => shopcore::buildUrl(T_CATEGORY, '', '', '', $this->oCategory->iId, '', false) );
		}

		# check search
		if ( is_object($this->oSearch) )
		{
			if ( strlen($this->sKeyword)>0 )
			{
			    $this->aBreadCrumb[] = array ('sName' => $this->sKeyword, 'sUrl' => shopcore::buildUrl(T_SEARCH, $this->sKeyword, '', '', '', '', false) );
			}
		}

		# check merchant
		if ( is_object($this->oMerchant) )
		{
			if ( is_numeric($this->oMerchant->iId) )
			{
			    $this->aBreadCrumb[] = array ('sName' => $this->oMerchant->sName, 'sUrl' => shopcore::buildUrl(T_CATEGORY, '', '', $this->oMerchant->iId, $this->oCategory->iId, '', false) );
			}
		}

		# modified clone of the above: specific to the product page
		# since now $this->oMerchant became $this->oProduct->oMerchant instead
		if ( is_object($this->oProduct->oMerchant) )
		{
			if ( is_numeric($this->oProduct->oMerchant->iId) )
			{
			    $this->aBreadCrumb[] = array ('sName' => $this->oProduct->oMerchant->sName, 'sUrl' => shopcore::buildUrl(T_CATEGORY, '', '', $this->oProduct->oMerchant->iId, $this->oCategory->iId, '', false) );
			}
		}



		# check product last!
		if ( is_object($this->oProduct) )
		{
			if ( is_numeric($this->oProduct->iId) )
			{
			    $this->aBreadCrumb[] = array ('sName' => $this->oProduct->sName, 'sUrl' => shopcore::buildUrl(T_PRODUCT, '', $this->oProduct->iId, $this->oProduct->iMerchantId, '', '', false) );
			}
		}

		return $this->aBreadCrumb;
	}



	/**
	 * Deduplicate and append refine by pair values to an array class var
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 		object $sRefineByQuery the parameters object
	 *
	 * @return 		array	the pairs as array with [defId]=>valId
	 */
	function setRefineByQuery($sRefineByQuery)
	{
		$aValues= array();

		//e.g. rb=2-23,1-20
		$aPairs= explode(',', $sRefineByQuery);

		foreach ($aPairs as $sPair) {
			$aParts= explode('-', $sPair);

			// remove definitions where value was 0
			if ($aParts[1]==0) {
				unset($aValues[$aParts[0]]);
			}
			else {
				$aValues[$aParts[0]]= $aParts[1];
			}
		}

		// combine the arrays preserving keys
		$this->aRefineByActiveVals+= $aValues;

		return $this->aRefineByActiveVals;
	}



	/**
	 * Gets the query string of the refine by pairs array
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 		object $sRefineByQuery the parameters object
	 *
	 * @return 		array
	 */
	function getRefineByQuery()
	{
		$aValues= array();

		foreach ($this->aRefineByActiveVals as $k=>$v) {
			$aValues[]= $k.'-'.$v;
		}

		$sValues= implode(',', $aValues);

		return $sValues;
	}



	/**
	 * Getters and setters
	 */
	function getDb()
	{
		return $this->_db;
	}
	function setDb($db)
	{
		$this->_db = $db;
	}
	function setMerchantId($iId)
	{
		$this->iMerchantId = $iId;
	}
	function getMerchantId()
	{
		return $this->iMerchantId;
	}
	function setCategoryId($iId)
	{
		$this->iCategoryId = $iId;
	}
	function getCategoryId()
	{
		return $this->iCategoryId;
	}
	function getKeyword()
	{
		return $this->sKeyword;
	}
	function getProductId()
	{
		return $this->iProductId;
	}
	function getAffiliateId()
	{
		return USER_ID;
	}





} // class


?>
