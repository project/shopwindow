<?php
/**
*
* ShopWindow Toolset
*
* Copyright (C) 2007 Digital Window Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.shopcore.php');
require_once('class.api_client.php');



class pageIndex extends shopcore
{
	var $iCategoryId= 		0;
	var $aMerchantIds= 		array();
	var $bTreeEmptyCats= 	false;


    /**
    * Constructor
    *
    * @access    public
    * @copyright affiliatewindow
    * @author    PAI <paul@affiliatewindow.com>
    */
    function pageIndex()
    {
        $this->setPageHtml();
    }

    /**
    * get page data requested
    *
    * @access    public
    * @copyright affiliatewindow
    * @author    PAI <paul@affiliatewindow.com>
    */
    function buildData()
    {
        # check requirements
        if ( !is_numeric($this->iCategoryId) ) $this->iCategoryId = 0;


        $oClient = new api_client();

        # category tree
        $aCategoryTreeParams = array("iCategoryId"=>$this->iCategoryId, "aMerchantIds"=>$this->aMerchantIds, "bIncludeEmptyCats"=>$this->bTreeEmptyCats, "bIncludeCounts"=>TREE_COUNTS);

        $this->oCategoryList = $oClient->call('getCategoryTree', $aCategoryTreeParams);


        # get category obj
        if ( $this->iCategoryId > 0 ) {
            $aCategoryParams = array('aCategoryIds'=>array($this->iCategoryId));
            // SOAP
            $oCategory = $oClient->call('getCategory', $aCategoryParams);
            $this->oCategory = $oCategory->getCategoryReturn[0];
        }


        # hot picks
        $aHotPickParams = array("iLimit"=>3);

        $this->oHotPick = $oClient->call('getHotPickProductList', $aHotPickParams);
    }

}
# end of class

?>
