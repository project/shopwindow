<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'soap'.DIRECTORY_SEPARATOR.'constants.inc.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'soap'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.ClientFactory.php');



/**
 * Extends SoapClient and automatically configures settings specific to the AWin API
 *
 */
class api_client
{
	var $oSoapClient; 		// the actuall SOAP client
	var $oResponse;			// the response object, after a succesfull call
	var $oSoapError;		// the SoapError object, if the call produced one



	/**
	 * The Constructor
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param  void
	 * @return object
	 */
	function api_client()
	{
		$this->oSoapClient= ClientFactory::getClient(PS_USERID, PS_USERPASS, PS_USERTYPE);
	}



	/**
	 * A wrapper for the actual SOAP function call. Also does some basic debugging/error printing
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 	string 	$sFunctionName
	 * @param 	mixed	$mParams
	 * @return 	object	the SOAP result / fault
	 */
	function call($sFunctionName, $mParams='')
	{

		$oResponse= $this->oSoapClient->call($sFunctionName, $mParams);

		// check for SoapFaults
		if ( is_a($oResponse, 'SoapError') ) {

			$this->oSoapError= $oResponse;

			// print SoapFaults
			if (PS_PRINT_SOAP_ERRORS==true) {
				print "<pre>".print_r($this->oSoapError)."</pre>";
			}
		}
		else {
			$this->oResponse= $oResponse;
		}


		// DEBUG
		if (PS_PRINT_SOAP_DEBUG==true) {
        	print '<h3>__getLastRequest('.$sFunctionName.')</h3>';
        	print '<pre>'.print_r($this->oSoapClient->__getLastRequest()).'</pre>';
	        print '<h3>__getLastResponse('.$sFunctionName.')</h3>';
	        print '<pre>'.print_r($this->oSoapClient->__getLastResponse()).'</pre>';
        }

		return $oResponse;
	}



} // end class

?>
