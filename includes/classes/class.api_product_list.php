<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.api_client.php');


/**
 * Used for modular implementation, class to encapsulate the corresponding API functionality
 *
 */
class api_productList extends api_client
{
	var $iCategoryId=			0;			// the category Id to which the categories tree belong
	var $aMerchantIds= 			array(); 	// the number of merchants to be returned
	var $sRefineByPairs=		'';			// refineBy id pairs e.g.(33-45,67-43,12-32)
	var $sSort=					'popular';	// one of [az, za, hi, lo, popular]
	var $iLimit=				10;			// the maximum number of products
	var $iOffset=				0;			// the offset on the list
	var $bIncludeDescendants=	true; 	 	// show products from within children categories



	/**
	 * Gets a list of products
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 	int 	 $oParams->iCategoryId
	 * @param 	array	 $oParams->aMerchantIds
	 * @param 	string 	 $oParams->sRefineByPairs
	 * @param 	string 	 $oParams->sSort
	 * @param 	int		 $oParams->iLimit
	 * @param 	int		 $oParams->iOffset
	 * @param 	bool	 $oParams->bIncludeDescendants
	 * @return 	array 	 of products
	 */
	function getProductList($oParams)
	{
		// sanity checks
		$this->iCategoryId= 	 is_numeric($oParams->iCategoryId)	 ? $oParams->iCategoryId	  : $this->iCategoryId;
		$this->aMerchantIds= 	 is_array($oParams->aMerchantIds) 	 ? $oParams->aMerchantIds	  : $this->aMerchantIds;
		$this->sRefineByPairs=   !empty($oParams->sRefineByPairs) 	 ? $oParams->sRefineByPairs   : $this->sRefineByPairs;
		$this->sSort= 			 !empty($oParams->sSort) 			 ? $oParams->sSort			  : $this->sSort;
		$this->iLimit= 			 is_numeric($oParams->iLimit) 		 ? $oParams->iLimit 		  : $this->iLimit;
		$this->iOffset= 		 is_numeric($oParams->iOffset) 		 ? $oParams->iOffset		  : $this->iOffset;
		$this->bIncludeDescendants= is_bool($oParams->bIncludeDescendants) ? $oParams->bIncludeDescendants : $this->bIncludeDescendants;


        $aParams= array('iCategoryId' => $this->iCategoryId,
        				'aMerchantIds' => $this->aMerchantIds,
        				'sRefineByPairs' => $this->sRefineByPairs,
        				'sSort' => $this->sSort,
        				'iLimit' => $this->iLimit,
        				'iOffset' => $this->iOffset,
        				'bIncludeDescendants' =>$this->bIncludeDescendants);


       	// make the SOAP call
        $this->call('getProductList', $aParams);

        $aProducts= $this->oResponse->getProductListReturn;


        return $aProducts;
	}

}


?>