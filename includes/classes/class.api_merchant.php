<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.api_client.php');


/**
 * Used for modular implementation, class to encapsulate the corresponding API functionality
 *
 */
class api_merchant extends api_client
{
	var $aMerchantIds= 		array(); 	// the ids of merchants to be returned



	/**
	 * Gets an array of merchant(s)
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 	array	 $oParams->aMerchantIds
	 * @return 	array 	 of merchants
	 */
	function getMerchant($oParams)
	{
		// sanity checks
		if (is_array($oParams->aMerchantIds)) {
			$this->aMerchantIds= array_unique($oParams->aMerchantIds);
		}
		else {
			return false;
		}

        $aParams= array('aMerchantIds' => $this->aMerchantIds);


       	// make the SOAP call
        $this->call('getMerchant', $aParams);


        $aMerchants= array();

        // re-assign to be keyed by merchant id
        foreach ($this->oResponse->getMerchantReturn as $oMerchant) {
 			$aMerchants[$oMerchant->iId]= $oMerchant;
        }


        return $aMerchants;
	}

}


?>