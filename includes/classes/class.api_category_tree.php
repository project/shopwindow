<?php
/**
*
* ShopWindow Toolset
*
* Copyright (C) 2007 Digital Window Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.api_client.php');


/**
 * Used for modular implementation, class to encapsulate the corresponding API functionality
 *
 */
class api_categoryTree extends api_client
{
	var $iCategoryId=			0;				// the category Id to which the categories belong
	var $aMerchantIds= 			array(); 		// the number of merchants to be returned
	var $sRelationship=			'all';			// one of: [all, biological, twinned, related]
	var $iMaxCategories=		9999;			// the maximum number of categories
	var $bIncludeEmptyCats=		false; 	 		// if empty categories should be returned
	var $bIncludeDescendants=	true; 	 		// if children/grandchildren categories should be returned
	var $bIncludeCounts=		false; 	 		// if category counts should be returned




	/**
	 * Gets the category tree
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 	int 	 $oParams->iCategoryId
	 * @param 	array	 $oParams->aMerchantIds
	 * @param 	string 	 $oParams->sRelationship
	 * @param 	int 	 $oParams->iMaxCategories
	 * @param 	bool	 $oParams->bIncludeEmptyCats
	 * @param 	bool 	 $oParams->bIncludeDescendants
	 * @param 	bool 	 $oParams->bIncludeCounts
	 * @return 	array	 of categories
	 */
	function getCategoryTree($oParams='')
	{
		// sanity check
		$this->iCategoryId=		  is_numeric($oParams->iCategoryId)	 	? $oParams->iCategoryId 	  : $this->iCategoryId;
		$this->aMerchantIds=	  is_array($oParams->aMerchantIds)	 	? $oParams->aMerchantIds 	  : $this->aMerchantIds;
		$this->sRelationship= 	  !empty($oParams->sRelationship)	 	? $oParams->sRelationship 	  : $this->sRelationship;
		$this->iMaxCategories= 	  is_numeric($oParams->iMaxCategories) 	? $oParams->iMaxCategories    : $this->iMaxCategories;
		$this->bIncludeEmptyCats= is_bool($oParams->bIncludeEmptyCats)  ? $oParams->bIncludeEmptyCats : $this->bIncludeEmptyCats;
		$this->bIncludeDescendants= is_bool($oParams->bIncludeDescendants) ? $oParams->bIncludeDescendants : $this->bIncludeDescendants;
		$this->bIncludeCounts=	  is_bool($oParams->bIncludeCounts)	 	? $oParams->bIncludeCounts 	  : $this->bIncludeCounts;


        $aParams= array('iCategoryId'=> $this->iCategoryId,
        				'aMerchantIds'=> $this->aMerchantIds,
        				'sRelationship'=> $this->sRelationship,
        				'iMaxCategories'=> $this->iMaxCategories,
        				'bIncludeEmptyCats'=> $this->bIncludeEmptyCats,
        				'bIncludeDescendants'=> $this->bIncludeDescendants,
        				'bIncludeCounts'=> $this->bIncludeCounts);


       	// make the SOAP call
        $this->call('getCategoryTree', $aParams);


        $aCategories= array();

        foreach($this->oResponse->getCategoryTreeReturn as $oCategory) {
			$aCategories[$oCategory->iId] = $oCategory;
		}


        return $aCategories;
	}

}


?>