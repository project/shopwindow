<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.api_client.php');


/**
 * Used for modular implementation, class to encapsulate the corresponding API functionality
 *
 */
class api_featuredMerchants extends api_client
{
	var $iCategoryId=	0;	// the category Id to which the merchants belong
	var $iLimit= 		5;  // the number of merchants to be returned



	/**
	 * Gets featured merchants for a category
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 	int 	 $oParams->iCategoryId
	 * @param 	int		 $oParams->iLimit
	 * @return 	array	 array of merchant objects
	 */
	function getFeaturedMerchants($oParams)
	{
		# sanity check and class var assignments
		$this->iCategoryId= is_numeric($oParams->iCategoryId) 	? $oParams->iCategoryId : $this->iCategoryId;
		$this->iLimit= 		is_numeric($oParams->iLimit) 		? $oParams->iLimit 		: $this->iLimit;


		// SOAP params
        $aParams= array("iCategoryId"=>$this->iCategoryId,
        				"iLimit"=>$this->iLimit);


       	// make the SOAP call
        $oResponse= $this->call('getFeaturedMerchants', $aParams);

		// array of merchants
        $aMerchants= $this->oResponse->getFeaturedMerchantsReturn;


        return $aMerchants;
	}

}


?>