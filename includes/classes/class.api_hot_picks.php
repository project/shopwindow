<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once('class.api_client.php');


/**
 * Used for modular implementation, class to encapsulate the corresponding API functionality
 *
 */
class api_hotPicks extends api_client
{
	var $iCategoryId=	false;		// the category Id to which the products belong
	var $aMerchantIds=	array();	// the merchant ids to restrict results to
	var $sSort= 		'popular';	// one of: [random, popular]
	var $iLimit= 		2;  		// the maximum number of products




	/**
	 * Gets featured products for a category
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param 	int 	 $oParams->iCategoryId
	 * @param 	array	 $oParams->aMerchantIds
	 * @param 	string	 $oParams->sSort
	 * @param 	int		 $oParams->iLimit
	 * @return 	array	 array of products
	 */
	function getHotPicks($oParams)
	{
		# sanity check and class var assignments
		$this->iCategoryId= 	is_numeric($oParams->iCategoryId)	? $oParams->iCategoryId 	: $this->iCategoryId;
		$this->aMerchantIds= 	is_array($oParams->aMerchantIds) 	? $oParams->aMerchantIds 	: $this->aMerchantIds;
		$this->sSort= 			!empty($oParams->sSort) 			? $oParams->sSort 			: $this->sSort;
		$this->iLimit= 			is_numeric($oParams->iLimit) 		? $oParams->iLimit 			: $this->iLimit;


		// SOAP params
        $aParams= array("iCategoryId"=> $this->iCategoryId,
        				"aMerchantIds"=> $this->aMerchantIds,
        				"sSort"=> $this->sSort,
        				"iLimit"=> $this->iLimit);


       	// make the SOAP call
        $this->call('getHotPickProductList', $aParams);


		// array of products
        $aProducts= $this->oResponse->getHotPickProductListReturn;


        return $aProducts;
	}

}


?>