<?php
/**
*
* ShopWindow Toolset
*
* Copyright (C) 2007 Digital Window Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


ini_set('display_errors', 0); // php errors


# DETERMINE SYSTEM SETTINGS, depending on OS
if (!defined('HOME_PATH')) {
	$sPath= str_replace('includes', '', dirname(__FILE__));

	define('HOME_PATH', $sPath);
}

# automatically determine the HOME_URL
if (!defined('HOME_URL')) {

	$aDirs= explode('/', $_SERVER['SCRIPT_NAME']);
	$sHomeUrl= 'http://'.$_SERVER['HTTP_HOST'];

	for ($i=0; $i<count($aDirs)-1; $i++) {
		$sHomeUrl.= $aDirs[$i].'/';
	}

	define('HOME_URL', $sHomeUrl);
}

# get settings from Smarty ini file
//$aSmartyIni= @parse_ini_file(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'smarty'.DIRECTORY_SEPARATOR.'configs'.DIRECTORY_SEPARATOR.'config.inc');
//strtolower($aSmartyIni['categoryTreeCounts'])=='yes' ? define('TREE_COUNTS', true) : define('TREE_COUNTS', false);


# template names
define('T_GOTO', HOME_URL.'goto.php');
define('T_INDEX', HOME_URL.'index.php');
define('T_SEARCH', HOME_URL.'search.php');
define('T_PRODUCT', HOME_URL.'product.php');
define('T_CATEGORY', HOME_URL.'category.php');


# product click url
define('PCLICK_URL', 'http://www.awin1.com/pclick.php');
define('AWCLICK_URL', 'http://www.awin1.com/awclick.php');


# from this weight percentage onwards, the user will be redirected automatically
# to the matching category, when performing a search
define('CATEGORY_REDIRECT_WEIGHT', 60);

?>
