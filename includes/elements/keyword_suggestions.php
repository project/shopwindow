<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


$aKeywordSuggestions = $oPage->oSearch->aKeywordSuggestions;

$aNewKeywordSuggestions = array();

// only if there's something to show
if (is_array($aKeywordSuggestions) && count($aKeywordSuggestions)>0) {
	foreach( $aKeywordSuggestions as $sWordSuggestion )
	{
		$oKeyword= new stdClass();
		$oKeyword->sKeyword = $sWordSuggestion;
		$oKeyword->sSearchLink = shopcore::buildUrl(T_SEARCH, $sWordSuggestion, '', '', $oPage->getCategoryId());

		$aNewKeywordSuggestions[] = $oKeyword;
	}
}


# SMARTY ASSIGN
$oSmarty->assign('aKeywordSuggestions', $aNewKeywordSuggestions);
$oSmarty->assign('iCategoryId', $oPage->getCategoryId());

?>
