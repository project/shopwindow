<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


$aRefineByDefs= $oPage->aRefByDefinitions;


# Get the name of the current page only
$sScriptName= HOME_URL.preg_replace('/\/.*\//', '', $_SERVER['SCRIPT_NAME']);


# Get previous refine by
$sRefineByQuery= $oPage->getRefineByQuery();

if (!empty($sRefineByQuery)) {
	$sRefineByQuery.= ',';
}



# Construct the query part for each value
# Loop through each array as per reference to work in PHP4 as well as PHP5
$aRefineByDefsKeys= array_keys($aRefineByDefs);

foreach ($aRefineByDefsKeys as $key_d) {

	// assign object by reference
	$oRefineByDef =& $aRefineByDefs[$key_d];

	// passing zero unsets that definition
	$sCancelRefineByLink= $sRefineByQuery.$oRefineByDef->iId."-0";

	$oRefineByDef->sCancelRefineByLink= shopcore::buildUrl($sScriptName, '', '', $oPage->iMerchantId, $oPage->getCategoryId(), '', $sCancelRefineByLink);


	$aRefineByValuesKeys= array_keys($oRefineByDef->aRefineByValues);

	foreach ($aRefineByValuesKeys as $key_v) {
		// assign object by reference
		$oRefByValue =& $oRefineByDef->aRefineByValues[$key_v];

		$sRefineByValueQuery= $sRefineByQuery.$oRefineByDef->iId."-".$oRefByValue->iId;

		// add the link property
		$oRefByValue->sRefineByLink= shopcore::buildUrl($sScriptName, '', '', $oPage->iMerchantId, $oPage->getCategoryId(), '', $sRefineByValueQuery);

		// unset reference
		unset($oRefByValue);
	}

	// unset reference
	unset($oRefineByDef);
}



# SMARTY ASSIGN
$oSmarty->assign('aRefineByDefs', $aRefineByDefs);
// the active values, already passed by query before
$oSmarty->assign('aRefineByActiveValueIds', array_values($oPage->aRefineByActiveVals));


?>