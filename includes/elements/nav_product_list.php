<?php


/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.smarty_sw.php');


// decide on the source of the product list
if (is_object($oPage->oSearch)) {
	$oProductListResponse= $oPage->oSearch;
	$iReturnedProducts= count($oPage->oSearch->searchProductReturn);
}
elseif (is_object($oPage->oProductList)) {
	$oProductListResponse= $oPage->oProductList;
	$iReturnedProducts= count($oPage->oProductList->getProductListReturn);
}
else {
	$oProductListResponse= $oProductList->oResponse;
	$iReturnedProducts= count($aProductsLoop);
}


$iListOffset =   is_numeric($_GET['iListOffset'])	? $_GET['iListOffset']  : $oProductListResponse->iOffset;
$iListLimit = 	is_numeric($_GET['iListLimit']) 	? $_GET['iListLimit'] 	: $oProductListResponse->iLimit;
$sListSort = 	strlen($_GET['sListSort'])>0 		? $_GET['sListSort'] 	: $oProductListResponse->sSort;
$sQuery = 	arg(2);	//strlen(arg(2))>0 				? arg(2) 			: $oProductListResponse->sQuery;
$iCategoryId =  arg(1);//is_numeric(arg(1)) 				? arg(1) 			: $oProductListResponse->$iCategoryId;


// used further down and for Smarty
$sCurrentPage= 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'];


# Navigation
if ( $oProductListResponse->iTotalCount> $oProductListResponse->iLimit )
{
	// remove previous limit
	// swap query string for request_uri

	$sPageQuery= preg_replace('/&?iListOffset=\d*/', '', $_SERVER['REQUEST_URI']);
	// remove image submit co-ordinates crap
	$sPageQuery= preg_replace('/&?search\.[x|y]=\d*/', '', $sPageQuery);


	// see if there were params left in query
	if (strlen($sPageQuery)>0) {
		$sPageUrl= $sCurrentPage.'?'.$sPageQuery.'&iListOffset=';
	}
	else {
		$sPageUrl= $sCurrentPage.'?iListOffset=';
	}

	//if ($_GET['q']==$_SERVER['REQUEST_URI']) {
	// drupal clean urls
		$sPageUrl= $sPageQuery.'&iListOffset=';
	
//	} else {
//	  $sPageUrl="?q=".$_GET['q']."&iListOffset=";
//	}
	

	# Draw the previous href
	if ( $oProductListResponse->iOffset>0 )	{
		$iPrevious = ( ($oProductListResponse->iOffset - $oProductListResponse->iLimit) < 0 ? '0' : ($oProductListResponse->iOffset - $oProductListResponse->iLimit) );
// prevent dupe content - eg iListOffset=0
    if ($iPrevious!=0)	$sPreviousLink= $sPageUrl.$iPrevious;
		else $sPreviousLink=$sPageQuery;
	}

	# Draw the next href
	if ( ($oProductListResponse->iOffset + $oProductListResponse->iLimit) < $oProductListResponse->iTotalCount ) {
		$iNext = ( $oProductListResponse->iLimit+$oProductListResponse->iOffset );

		$sNextLink= $sPageUrl.$iNext;
	}


	# Pagenator
	$aPagenator= array();
	$iOffset= 0;

	// set starting point
	$i= !empty($oProductListResponse->iOffset) ? ($oProductListResponse->iOffset/$oProductListResponse->iLimit) : 1;
	// keep selected page in the middle
	$i= ($i-3)<0 ? 0 : $i-3;

	$iLast= $i+10; // 10 is the number of visible page links

	while ($i<$iLast && $iOffset< $oProductListResponse->iTotalCount) {
		$iOffset= ($i-1)*$oProductListResponse->iLimit;
		$aPagenator[$i]= $sPageUrl.$iOffset;
		$i++;
	}

	// there's always one extra at the end so pop it
	array_pop($aPagenator);
	// and sometimes at the beginning
	unset($aPagenator[0]);

	// overwrite the first page link
	// again lets scrap this, and fix dupe content
//	$aPagenator[1]= $sPageUrl.'0';
	$aPagenator[1]= $sPageQuery;
	// add the last page link
	$iLastPage= @floor($oProductListResponse->iTotalCount / $oProductListResponse->iLimit);
	// if there's no remainder, that means the previous page will contain tha last product
	// therefore remove the last page, which would have been empty anyway
	$iLastPage= @($oProductListResponse->iTotalCount % $oProductListResponse->iLimit)==0 ?  $iLastPage-1 : $iLastPage;

	$aPagenator[$iLastPage+1]= $sPageUrl.($iLastPage * $oProductListResponse->iLimit);

	// sort ASC
	ksort($aPagenator);
}


# Construct values for the limit select box
$aSelectLimits= array('4'=>4, '10'=>10,'20'=>20,'30'=>30,'40'=>40,'50'=>50);
// assign current value as well (dynamic for modular)
if (is_numeric($oProductListResponse->iLimit)) {
	$aSelectLimits[$oProductListResponse->iLimit]= $oProductListResponse->iLimit;
}
// remove dups and sort
$aSelectLimits= array_unique($aSelectLimits);
ksort($aSelectLimits);


# Construct values for the sort select box
$aSelectSorts = array();

// add relevance to the box if we are on the search page
if ( $sCurrentPage==T_SEARCH ) $aSelectSorts['relevance'] = 'Relevance';

$aSelectSorts['popular'] = 'Popularity';
$aSelectSorts['lo'] = 'Price: Low-High';
$aSelectSorts['hi'] = 'Price: High-Low';
$aSelectSorts['az'] = 'Name: A-Z';
$aSelectSorts['za'] = 'Name: Z-A';


// so we know which page to highlight
$iCurrentPage= ($oProductListResponse->iOffset/$oProductListResponse->iLimit)+1;



# SMARTY ASSIGN

// decide which page are we at
/*
if ($oProductDisplayListParams->bStayOnPage== true) {
	$oSmarty->assign('PRODUCTLIST_PAGE', $sCurrentPage);
}
elseif ($sCurrentPage==T_SEARCH) {
	$oSmarty->assign('PRODUCTLIST_PAGE', T_SEARCH);
}
else {
	$oSmarty->assign('PRODUCTLIST_PAGE', T_CATEGORY);
}
*/
	$oSmarty->assign('PRODUCTLIST_PAGE', $sCurrentPage);

$oSmarty->assign('sQuery', stripslashes($sQuery));
$oSmarty->assign('iCategoryId', $iCategoryId);
$oSmarty->assign('iMerchantId', $iMerchantId);

$oSmarty->assign('aSelectLimits', $aSelectLimits);
$oSmarty->assign('aSelectSorts', $aSelectSorts);

$oSmarty->assign('iCurrentPage', $iCurrentPage);
$oSmarty->assign('iListOffset', $iListOffset);
$oSmarty->assign('iListLimit', $iListLimit);
$oSmarty->assign('sListSort', $sListSort);

$oSmarty->assign('sPreviousLink', $sPreviousLink);
$oSmarty->assign('sNextLink', $sNextLink);
$oSmarty->assign('aPagenator', $aPagenator);

$oSmarty->assign('iTotalProducts', $oProductListResponse->iTotalCount);
$oSmarty->assign('iPageProducts', $oProductListResponse->iOffset + $iReturnedProducts);


// getting called from within "product_display_list.php"
if ($bAutonomy===true) {
	if ($oProductDisplayListParams->bShowTopNav== true) {
		$oSmarty->display('elements'.DIRECTORY_SEPARATOR.'nav_product_list_top.tpl');
	}
	elseif ($oProductDisplayListParams->bShowBottomNav== true) {
		$oSmarty->display('elements'.DIRECTORY_SEPARATOR.'nav_product_list_bottom.tpl');
	}
}

?>