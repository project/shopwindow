<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_category_tree.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_category.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.smarty_sw.php');


$aSuperCats= array();

# Autonomy
// page mode
if (isset($oPage) && !isset($oApiCategoryTree)) {

	$aCats = $oPage->oCategoryList->getCategoryTreeReturn;
	$oCurrentCategory= $oPage->oCategory;
	$sQuery= $oPage->getKeyword();


	// get the super categories
	$oApiCategoryTree= new api_categoryTree();

	if ($oCurrentCategory->iId!=0) {
		$aSuperCats= $oApiCategoryTree->getCategoryTree();
	}
}
// autonomous mode
else {

	# set params with GET values if empty
	if ( empty($oSearchBoxParams->sQuery) && strlen(arg(2))>0 ) {
		$oSearchBoxParams->sQuery= arg(2);
	}

	if ( empty($oSearchBoxParams->iCategoryId) && is_numeric(arg(1)) ) {
		$oSearchBoxParams->iCategoryId= arg(1);
	}



	// set the search query
	$sQuery= $oSearchBoxParams->sQuery;



	// get the list
	$oApiCategoryTree= new api_categoryTree();

	$aCats= $oApiCategoryTree->getCategoryTree($oSearchBoxParams);

	// prepare params & get the category
	$oCategoryParams= new stdClass();
	$oCategoryParams->aCategoryIds= array($oApiCategoryTree->iCategoryId);

	$oApiCategory= new api_category();
	$aCategories= $oApiCategory->getCategory($oCategoryParams);
	$oCurrentCategory= $aCategories[$oApiCategoryTree->iCategoryId];



	// get the super categories
	if ($oCurrentCategory->iId!=0) {
		$oParams= new stdClass();
		$oParams->iCategoryId= 0;
		$aSuperCats= $oApiCategoryTree->getCategoryTree($oParams);
	}

	$oSmarty= new Smarty_SW();
	$bAutonomy= true; // flag
}


// unset the currently selected category, gets added further down as selected
unset($aSuperCats[$oCurrentCategory->iId]);


$aTopLevelCats= array();

// Used for dropdown
foreach($aCats as $oCat) {
	$aTopLevelCats[$oCat->iId] = $oCat->sName;
}

// add the super categories
if (count($aSuperCats)>0) {
	// add spacer & title
	$aTopLevelCats['spacer']= '&nbsp;';
	$aTopLevelCats['main_title']= '---- Main Categories ----';

	foreach($aSuperCats as $oCat) {
		$aTopLevelCats[$oCat->iId] = '&nbsp;&nbsp;'.$oCat->sName;
	}
}



# prepare the SELECT box

// reverse to add options to the beginning
$aTopLevelCats= array_reverse($aTopLevelCats, true);

if ( is_object($oCurrentCategory) ) {

	if ( is_numeric($oCurrentCategory->iId) && strlen($oCurrentCategory->sName)>0 )	{

		$aTopLevelCats['dashes1'] = '--------';
		$aTopLevelCats[$oCurrentCategory->iId] = '&nbsp;&nbsp;'.$oCurrentCategory->sName;
		$aTopLevelCats['dashes2'] = '--------';

		$sOptionSelected= $oCurrentCategory->iId;
	}
}

$aTopLevelCats[0]= 'All Categories';

// reverse back to normal order
$aTopLevelCats= array_reverse($aTopLevelCats, true);


# SMARTY ASSIGN
$oSmarty->assign('sSearchTerm', stripslashes($sQuery));
$oSmarty->assign('sOptionSelected', $sOptionSelected);
$oSmarty->assign('aTopLevelCats', $aTopLevelCats);


if ($bAutonomy===true) {
	// stay on the same page
	if ($oSearchBoxParams->bStayOnPage==true) {
		$oSmarty->assign('P_SEARCH', $_SERVER['SCRIPT_NAME']);
	}

	$oSmarty->display('elements'.DIRECTORY_SEPARATOR.'search_box.tpl');
}