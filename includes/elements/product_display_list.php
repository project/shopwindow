<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


//require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_product_list.php');

require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_product_list.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_search_product.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_merchant.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_category.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.smarty_sw.php');



# Autonomy
// page mode
if (isset($oPage) && !isset($oProductList)) {

	$aMerchants= $oPage->aMerchs;
	$aCategories= $oPage->aCats;
}
// autonomous mode
else {

	# set params with GET values if empty
	 /* this is never empty as drupal uses q
	if ( empty($oProductDisplayListParams->sQuery) && strlen($_GET['q'])>0 ) {
		$oProductDisplayListParams->sQuery= $_GET['q'];
	}
*/
	if ( empty($oProductDisplayListParams->iCategoryId) && is_numeric($_GET['c']) ) {
		$oProductDisplayListParams->iCategoryId= $_GET['c'];
	}

	if ( empty($oProductDisplayListParams->iMerchantId) && is_numeric($_GET['m']) ) {
		$oProductDisplayListParams->iMerchantId= array($_GET['m']);
	}

	if ( empty($oProductDisplayListParams->iOffset) && is_numeric($_GET['iListOffset']) ) {
		$oProductDisplayListParams->iOffset= $_GET['iListOffset'];
	}

	if ( empty($oProductDisplayListParams->iLimit) && is_numeric($_GET['iListLimit']) ) {
		$oProductDisplayListParams->iLimit= $_GET['iListLimit'];
	}

	if ( empty($oProductDisplayListParams->sSort) && strlen($_GET['sListSort'])>0 ) {
		$oProductDisplayListParams->sSort= $_GET['sListSort'];
	}



	// create the actual array of ids
	$oProductDisplayListParams->aMerchantIds= array($oProductDisplayListParams->iMerchantId);



	# Decide how to get the products
	// query was passed, so search
	if (!empty($oProductDisplayListParams->sQuery)) {
		$oProductList= new api_search_product();
		$aProductsLoop= $oProductList->searchProduct($oProductDisplayListParams);
	}
	// just list per category
	else {
		$oProductList= new api_productList();
		$aProductsLoop= $oProductList->getProductList($oProductDisplayListParams);
	}


	// get all the merchant ids we gonna need
	$oMerchantParams= new stdClass();
	$oCategoryParams= new stdClass();

	foreach ($aProductsLoop as $oProduct) {
		$oMerchantParams->aMerchantIds[]= $oProduct->iMerchantId;
		$oCategoryParams->aCategoryIds[]= $oProduct->iCategoryId;
		$oBrandParams->Brands[]=$oProduct->sBrand;
	}

	// prepare params & get the merchants
	$oMerchant= new api_merchant();
	$aMerchants= $oMerchant->getMerchant($oMerchantParams);

	// get the categories
	$oCategory= new api_category();
	$aCategories= $oCategory->getCategory($oCategoryParams);


	$oSmarty= new Smarty_SW();
	$bAutonomy= true; // flag
}




$aProducts= array();

foreach($aProductsLoop as $oProd)
{
	$oProduct= new stdClass();

	// check brand name is in the product name
	$sProdName= stripos($oProd->sName, $oProd->sBrand)===false ? $oProd->sBrand.' '.$oProd->sName : $oProd->sName;


	// pick an image to show
    $sProductImageUrl= !empty($oProd->sAwThumbUrl) ? $oProd->sAwThumbUrl : '';


	# build links
	/*
	$sProductLink= 		shopcore::buildUrl(T_PRODUCT, '', $oProd->iId, '', $oProd->iCategoryId);
	$sCategoryLink= 	shopcore::buildUrl(T_CATEGORY, '', '', '', $oProd->iCategoryId);
	$sMerchantLink= 	shopcore::buildUrl(T_PRODUCT, '', '', $oProd->iMerchantId);
	$sProductBuyLink= 	shopcore::buildUrl(T_GOTO, '', $oProd->iId, $oProd->iMerchantId);
*/
	$sProductBuyLink="jump/".$oProd->iId."/".$oProd->iMerchantId;
	$sProductLink="product/".$oProd->iId."/".$oProd->iCategoryId;
	$sProductLink=check_url(url($sProductLink, NULL, NULL, NULL));
	$sProductBuyLink=check_url(url($sProductBuyLink, NULL, NULL, NULL));
	

  $sCategoryLink="browse/".$oProd->iCategoryId;
	$sCategoryLink=check_url(url($sCategoryLink, NULL, NULL, NULL));

	$sMerchantLink="merchant/".$oProd->iMerchantId;
	$sMerchantLink=check_url(url($sMerchantLink, NULL, NULL, NULL));

	// for modular implementation
	/*
	if ($oProductDisplayListParams->bStayOnPage==true) {
		$sProductLink= 	str_replace(T_PRODUCT, $_SERVER['SCRIPT_NAME'], $sProductLink);
		$sCategoryLink=	str_replace(T_CATEGORY, $_SERVER['SCRIPT_NAME'], $sCategoryLink);
		$sMerchantLink= str_replace(T_PRODUCT, $_SERVER['SCRIPT_NAME'], $sMerchantLink);
	}
*/
	// add category info into the product obj
	$oProduct->sCategoryName = 			$aCategories[$oProd->iCategoryId]->sName;
	$oProduct->sCategoryLink = 			$sCategoryLink;

	$oProduct->fSearchPrice = 			$oProd->fSearchPrice;

	$oProduct->sProductImageUrl = 		$sProductImageUrl;
	$oProduct->sProductLink = 			$sProductLink;
	$oProduct->sProductBuyLink = 		$sProductBuyLink;
	$oProduct->sProductName = 			$oProd->sName;
	
	// insert charitable donation if on rectifi
	if (module_exists('charitysearch')) {
	    $com=get_commission_value($oProd->iMerchantId);
			if ($com>1) $raised=theme_commission_value($com);
			else {
			  $raised="&pound;".number_format($com*$oProd->fSearchPrice,2)." (".theme_commission_value($com).")";
   		}
			$oProd->sDesc="<div style=\"color:#f00\">Buy now and raise $raised for charity</span>.<br>".strip_tags($oProd->sDesc);
	}	
	$oProduct->sProductDescription = 	$oProd->sDesc;
	$oProduct->sProductPromo = 			$oProd->sPromo;

	$oProduct->sMerchantName = 			$aMerchants[$oProd->iMerchantId]->sName;
	$oProduct->sMerchantLink = 			$sMerchantLink;
	$oProduct->sMerchantLogoUrl = 		$aMerchants[$oProd->iMerchantId]->sLogoUrl;

	$aProducts[]= $oProduct;
}


# SMARTY ASSIGN
$oSmarty->assign('aProducts', $aProducts);


if ($bAutonomy===true && count($aProducts)>0) {

	if ($oProductDisplayListParams->bShowTopNav== true || $oProductDisplayListParams->bShowBottomNav== true) {
		include_once('nav_product_list.php');
	}

	$oSmarty->display('elements'.DIRECTORY_SEPARATOR.'product_display_list.tpl');
}

	if (!variable_get("shopwindow_link_categories",1)) $sCategoryLink="#";

?>