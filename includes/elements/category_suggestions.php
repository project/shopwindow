<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/
/*
        $aSearchParams = array('sQuery'=>$this->sKeyword, 'aMerchantIds' => $aMerchantIds );
        // SOAP
        $this->oCatSearch = $oClient->call('getSuggestedCategory', $aSearchParams);
*/
//$aCategorySuggestions = $oProductList->oCatSearch->getSuggestedCategoryReturn;

$aNewCategorySuggestions = array();
//print_r($oProductList);
print_r($aCategorySuggestions);
if (is_array($aCategorySuggestions) && count($aCategorySuggestions)>0) {
	foreach( $aCategorySuggestions as $oCat )
	{
		# if the weight is over 50% match, send the user direct
		// CATEGORY_REDIRECT_WEIGHT >> "shopwindow_cat_weight"
		if ( $oCat->iWeight> variable_get("shopwindow_cat_weight",60) )
		{
//		    header("Location: ".T_CATEGORY."?c=".$oCat->iId."&q=".$oPage->getKeyword());
//					drupal_goto("browse/".$oCat->iId."/".$oPage->getKeyword());
		}

		$oCategory= new stdClass();
		$oCategory->iId = $oCat->iId;
        $oCategory->sName = $oCat->sName;
        $oCategory->iWeight = $oCat->iWeight;
        $oCategory->iParentId = $oCat->iParentId;
				 $sCategoryLink="browse/".$oCat->iId;
	$sCategoryLink=check_url(url($sCategoryLink, NULL, NULL, NULL));
	$oCategory->sCategoryLink=$oCategory->sCategoryLink;
//		$oCategory->sCategoryLink = shopcore::buildUrl(T_CATEGORY, $oCat->sName, '', '', $oCategory->iId);

		$aNewCategorySuggestions[] = $oCategory;
	}
}

# SMARTY ASSIGN
$oSmarty->assign('aCategorySuggestions', $aNewCategorySuggestions);
$oSmarty->assign('sKeyword', $oProductList->getKeyword());

?>
