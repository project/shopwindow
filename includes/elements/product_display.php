<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_product.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_merchant.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_category.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.smarty_sw.php');



# Autonomy
// page mode
if (isset($oPage) && !isset($oApiProduct)) {
	$oProduct= $oPage->oProduct;
	$oMerchant= $oPage->oProduct->oMerchant;
	$oCategory= $oPage->oCategory;
}
// autonomous mode
else {

	# set params with GET values if empty
	if ( empty($oProductDisplayParams->iProductId) && is_numeric($_GET['p']) ) {
		$oProductDisplayParams->iProductId= $_GET['p'];
	}

	// create the actual array of ids
	$oProductDisplayParams->aProductIds= array($oProductDisplayParams->iProductId);



	// get the product
	$oApiProduct= new api_product();
	$aProducts= $oApiProduct->getProduct($oProductDisplayParams);
	$oProduct= $aProducts[$oProductDisplayParams->iProductId];

	// get the product's merchant
	$oApiMerchant= new api_merchant();
	$oParams= new stdClass();
	$oParams->aMerchantIds= array($oProduct->iMerchantId);
	$aMerchants= $oApiMerchant->getMerchant($oParams);
	$oMerchant= $aMerchants[$oProduct->iMerchantId];

	// get the product's category
	$oApiCategory= new api_category();
	$oParams= new stdClass();
	$oParams->aCategoryIds= array($oProduct->iCategoryId);
	$aCategories= $oApiCategory->getCategory($oParams);
	$oCategory= $aCategories[$oProduct->iCategoryId];


	$oSmarty= new Smarty_SW();
	$bAutonomy= true; // flag
}


// check brand name is in the product name
if ( stripos($oProduct->sName, $oProduct->sBrand) === false ) {
    $sProdName = $oProduct->sName.' by '.$oProduct->sBrand;
}
else {
    $sProdName = $oProduct->sName;
}


// pick an image to show
if ( strlen($oProduct->sAwImageUrl)>5 ) {
    $sProductImageUrl = $oProduct->sAwImageUrl;
}
else if ( strlen($oProduct->sImageUrl)>5 ) {
    $sProductImageUrl = $oProduct->sImageUrl;
}
else if ( strlen($oProduct->sAwThumbUrl)>5 ) {
    $sProductImageUrl = $oProduct->sAwThumbUrl;
}
else if ( strlen($oProduct->sThumbUrl)>5 ) {
    $sProductImageUrl = $oProduct->sThumbUrl;
}
else {
    $sProductImageUrl = '';
}



// build links
$sProductLink	= shopcore::buildUrl(T_GOTO, '', $oProduct->iId, $oProduct->iMerchantId, $oCategory->iId);
$sMerchantLink	= shopcore::buildUrl(T_GOTO, '', $oProduct->iId, $oProduct->iMerchantId);

	$sProductLink="/jump/".$oProduct->iId."/".$oProduct->iMerchantId;

# construct cat/link array.
$sCategoryLink= shopcore::buildUrl(T_CATEGORY, '', '', '', $oCategory->iId);

// for modular implementation
if ($oProductDisplayParams->bStayOnPage==true) {
	$sCategoryLink=	str_replace(T_CATEGORY, $_SERVER['SCRIPT_NAME'], $sCategoryLink);
}

  $sCategoryLink="/browse/".$oProduct->iCategoryId;
	$sCategoryLink=check_url(url($sCategoryLink, NULL, NULL, NULL));
//	$sMerchantLink="/merchant/".$oProduct->iMerchantId;
// looking at page makes far more sense for merchant link = product link
	$sMerchantLink= $sProductLink; // ="/jump/".$oProduct->iId."/".$oProduct->iMerchantId;

	if (!variable_get("shopwindow_link_categories",1)) $sCategoryLink="#";

# SMARTY ASSIGN
$oSmarty->assign('sProductCategoryName', $oCategory->sName);
$oSmarty->assign('sProductCategoryLink', $sCategoryLink);

$oSmarty->assign('fSearchPrice', $oProduct->fSearchPrice);

$oSmarty->assign('sProductImageUrl', $sProductImageUrl);
$oSmarty->assign('sProductLink', $sProductLink);
$oSmarty->assign('sProductName', $sProdName);
	// insert charitable donation if on rectifi
	if (module_exists('charitysearch')) {
	    $com=get_commission_value($oProduct->iMerchantId);
			if ($com>1) $raised=theme_commission_value($com);
			else {
			  $raised="&pound;".number_format($com*$oProduct->fSearchPrice,2)." (".theme_commission_value($com).")";
   		}
			$oProduct->sDesc=strip_tags($oProduct->sDesc)."<br><div style=\"color:#f00\">Buy now and raise $raised for charity</div><br>";
	}	

$oSmarty->assign('sProductDesc', $oProduct->sDesc);
$oSmarty->assign('sProductPromo', $oProduct->sPromo);

$oSmarty->assign('sMerchantName', $oMerchant->sName);
$oSmarty->assign('sMerchantLink', $sMerchantLink);
$oSmarty->assign('sMerchantLogoUrl', $oMerchant->sLogoUrl);


if ($bAutonomy===true && !empty($oProductDisplayParams->iProductId)) {
	$oSmarty->display('elements'.DIRECTORY_SEPARATOR.'product_display.tpl');
}

?>