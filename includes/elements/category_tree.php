<?php
/**
*
* ShopWindow Toolset
*
* Copyright (C) 2007 Digital Window Ltd.
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/

require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_category_tree.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_category.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_merchant.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.smarty_sw.php');


# Autonomy
// page mode
if (isset($oPage) && !isset($oCategoryTree)) {
	$aCats = $oPage->oCategoryList->getCategoryTreeReturn;

	// sometimes passed as restriction
	$iMerchantId= $oPage->oMerchant->iId;

	// get restricted by merchant
	if (!empty($iMerchantId)) {
		$oMerchant= $oPage->oMerchant;
	}

	// tree parent category
	$oParentCategory= $oPage->oCategory;
}
// autonomous mode
else {

	# set params with GET values if empty
	if ( empty($oCategoryTreeParams->iCategoryId) && is_numeric($_GET['c']) ) {
		$oCategoryTreeParams->iCategoryId= $_GET['c'];
	}

	if ( empty($oCategoryTreeParams->iMerchantId) && is_numeric($_GET['m']) ) {
		$oCategoryTreeParams->iMerchantId= array($_GET['m']);
	}



	// make sure we pass an int
	if (!is_numeric($oCategoryTreeParams->iCategoryId)) {
		$oCategoryTreeParams->iCategoryId= 0;
	}


	// stick into the actuall param array & keep for further use
	$oCategoryTreeParams->aMerchantIds= array($oCategoryTreeParams->iMerchantId);
	$iMerchantId= $oCategoryTreeParams->aMerchantIds[0];



	// get the list
	$oApiCategoryTree= new api_categoryTree();
	$aCats= $oApiCategoryTree->getCategoryTree($oCategoryTreeParams);

	// prepare params & get the parent category
	$oCategoryParams= new stdClass();
	$oCategoryParams->aCategoryIds= array($oApiCategoryTree->iCategoryId);
	$oApiCategory= new api_category();
	$aCategories= $oApiCategory->getCategory($oCategoryParams);

	// tree parent category
	$oParentCategory= $aCategories[$oApiCategoryTree->iCategoryId];

	// get restricted by merchant
	if (!empty($iMerchantId)) {
		$oMerchantParams= new stdClass();
		$oMerchantParams->aMerchantIds= array($iMerchantId);
		$oApiMerchant= new api_merchant();
		$aMerchants= $oApiMerchant->getMerchant($oMerchantParams);
		$oMerchant= $aMerchants[$iMerchantId];
	}

	$oSmarty= new Smarty_SW();
	$bAutonomy= true; // flag
}



// no tree cats for grandchild
// so get its parent tree instead to always show something
if (count($aCats)<1) {
	$oParams= new stdClass();
	$oParams->iCategoryId= $oParentCategory->iParentId;
	$oParams->bIncludeCounts= TREE_COUNTS;

	// get the list
	$oApiCategoryTree= new api_categoryTree();
	$aCats= $oApiCategoryTree->getCategoryTree($oParams);

	// prepare params & get the parent category
	$oCategoryParams= new stdClass();
	$oCategoryParams->aCategoryIds= array($oApiCategoryTree->iCategoryId);

	$oApiCategory= new api_category();
	$aCategories= $oApiCategory->getCategory($oCategoryParams);

	// overwrite the current tree parent category
	$oParentCategory= $aCategories[$oApiCategoryTree->iCategoryId];
}


// show restriction by merchant in the title
if ( !empty($iMerchantId) ) {
	$sCatLink= shopcore::buildUrl(T_CATEGORY, '', '', '', $oParentCategory->iId);

	// for modular implementation
	if ($oCategoryTreeParams->bStayOnPage==true) {
		$sCatLink= 	str_replace(T_CATEGORY, $_SERVER['SCRIPT_NAME'], $sCatLink);
	}

    $sCatExplained = ' from '.$oMerchant->sName.' (<a href="'.$sCatLink.'" >show all</a>)';
}

// tree title
$sTreeTitle= is_object($oParentCategory) ? $oParentCategory->sName : 'Category List';



$aCategories= array();

foreach($aCats as $oCat)
{
	$oCategory= new stdClass();

	// Object for Smarty
	$oCategory->iId = $oCat->iId;
	$oCategory->sName = $oCat->sName;
	  $sCategoryLink="browse/".$oCat->iId;
	$sCategoryLink=check_url(url($sCategoryLink, NULL, NULL, NULL));

	$oCategory->sLink = $sCategoryLink;
	$oCategory->sDescription = $oCat->sDescription;
	$oCategory->iParentId = $oCat->iParentId;
	$oCategory->iTotalCount = $oCat->iTreeCount + $oCat->iLocalCount;
	$oCategory->aThreeRandomGrandChildren = $oCat->aThreeRandomGrandChildren;
	$oCategory->aChildren = $oCat->aChildren;

	// for modular implementation
	/*
	We can skip this as we are modular and we're already doing our own thing.
	if ($oCategoryTreeParams->bStayOnPage==true) {
		$oCategory->sLink= str_replace(T_CATEGORY, $_SERVER['SCRIPT_NAME'], $oCategory->sLink);
	}
 */

	# Assign variables on spot by referencing original objects
	// create grandchildren link
	if (is_array($oCategory->aThreeRandomGrandChildren)) {
		while (list($key)= each($oCategory->aThreeRandomGrandChildren)) {
		   $oGrandCat=& $oCategory->aThreeRandomGrandChildren[$key];
			  $sCategoryLink="browse/".$oGrandCat->iId;
	$sCategoryLink=check_url(url($sCategoryLink, NULL, NULL, NULL));
		   $oGrandCat->sLink= $sCategoryLink;
		   $oGrandCat->iTotalCount = $oGrandCat->iTreeCount + $oGrandCat->iLocalCount;

		   // for modular implementation
			 /*
		   if ($oCategoryTreeParams->bStayOnPage==true) {
		   		$oGrandCat->sLink= str_replace(T_CATEGORY, $_SERVER['SCRIPT_NAME'], $oGrandCat->sLink);
		   }
*/
		   unset($oGrandCat);
		}
	}

	// create children link
	if (is_array($oCategory->aChildren)) {
		while (list($key)= each($oCategory->aChildren)) {
		   $oChildCat=& $oCategory->aChildren[$key];
	 $sCategoryLink="browse/".$oChildCat->iId;
	$sCategoryLink=check_url(url($sCategoryLink, NULL, NULL, NULL));
		   $oChildCat->sLink= $sCategoryLink;
		   $oChildCat->iTotalCount = $oChildCat->iTreeCount + $oChildCat->iLocalCount;

		   // for modular implementation
			 /*
		   if ($oCategoryTreeParams->bStayOnPage==true) {
		   		$oChildCat->sLink= str_replace(T_CATEGORY, $_SERVER['SCRIPT_NAME'], $oChildCat->sLink);
		   }
*/
		   unset($oChildCat);
		}
	}


	// Finalize the categories array
	$aCategories[] = $oCategory;
}



# SMARTY ASSIGN
$oSmarty->assign('sTreeTitle', $sTreeTitle);
$oSmarty->assign('sCatExplained', $sCatExplained);
$oSmarty->assign('aCategories', $aCategories);


if ($bAutonomy===true) {
	$oSmarty->display('elements'.DIRECTORY_SEPARATOR.'category_tree.tpl');
}


?>