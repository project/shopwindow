<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_featured_merchants.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.smarty_sw.php');



# Autonomy
// page mode
if (isset($oPage) && !isset($oFeaturedMerchants)) {
	$aFeaturedMerchants= $oPage->oFeaturedMerchants->getFeaturedMerchantsReturn;
	$iCategoryId= $oPage->getCategoryId();
}
// autonomous mode
else {

	# set params with GET values if empty
	if ( empty($oFeaturedMerchantsParams->iCategoryId) && is_numeric($_GET['c']) ) {
		$oFeaturedMerchantsParams->iCategoryId= $_GET['c'];
	}


	$oFeaturedMerchants= new api_featuredMerchants();
	$aFeaturedMerchants= $oFeaturedMerchants->getFeaturedMerchants($oFeaturedMerchantsParams);
	$iCategoryId= $oFeaturedMerchants->iCategoryId;

	$oSmarty= new Smarty_SW();
	$bAutonomy= true; // flag
}



$aNewFeaturedMerchants= array();

foreach($aFeaturedMerchants as $oMerchant)
{
	$oNewMerchant= new stdClass();
	$oNewMerchant->iId= $oMerchant->iId;
	$oNewMerchant->sName= $oMerchant->sName;
	$oNewMerchant->sLogoUrl= $oMerchant->sLogoUrl;
	$oNewMerchant->sLink= $oMerchant->sClickThroughUrl;
	$oNewMerchant->sCategoryMerchantLink= shopcore::buildUrl(T_CATEGORY, '', '', $oMerchant->iId, $iCategoryId);

	// for modular implementation
	if ($oFeaturedMerchantsParams->bStayOnPage==true) {
		$oNewMerchant->sCategoryMerchantLink= str_replace(T_CATEGORY, $_SERVER['SCRIPT_NAME'], $oNewMerchant->sCategoryMerchantLink);
	}

	$aNewFeaturedMerchants[]= $oNewMerchant;
}



# SMARTY ASSIGN
$oSmarty->assign('aFeaturedMerchants', $aNewFeaturedMerchants);
$oSmarty->assign('iCategoryId', $iCategoryId);


if ($bAutonomy===true) {
	$oSmarty->display('elements'.DIRECTORY_SEPARATOR.'featured_merchants.tpl');
}

?>