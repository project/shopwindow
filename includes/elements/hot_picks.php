<?php
/**
* 
* ShopWindow Toolset
* 
* Copyright (C) 2007 Digital Window Ltd.
* 
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public License
* as published by the Free Software Foundation; either version 2
* of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*
*/


require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.api_hot_picks.php');
require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'classes'.DIRECTORY_SEPARATOR.'class.smarty_sw.php');



# Autonomy
// page mode
if (isset($oPage) && !isset($oHotPicks)) {
	$aProds = $oPage->oHotPick->getHotPickProductListReturn;
}
// autonomous mode
else {

	# set params with GET values if empty
	if ( empty($oHotPicksParams->iCategoryId) && is_numeric($_GET['c']) ) {
		$oHotPicksParams->iCategoryId= $_GET['c'];
	}


	$oHotPicks= new api_hotPicks();
	$aProds= $oHotPicks->getHotPicks($oHotPicksParams);

	$oSmarty= new Smarty_SW();
	$bAutonomy= true; // flag
}



$aProducts= array();

foreach ( $aProds as $oProd )
{
	$oProduct= new stdClass();

	$sProductLink= shopcore::buildUrl(T_PRODUCT, '', $oProd->iId, '', $oProd->iCategoryId);

	// for modular implementation
	if ($oHotPicksParams->bStayOnPage==true) {
		$sProductLink= 	str_replace(T_PRODUCT, $_SERVER['SCRIPT_NAME'], $sProductLink);
	}
	$sProductBuyLink="jump/".$oProd->iId."/".$oProd->iMerchantId;
	$sProductLink="product/".$oProd->iId."/".$oProd->iCategoryId;
	
	$sProductLink=check_url(url($sProductLink, NULL, NULL, NULL));
	$sProductBuyLink=check_url(url($sProductBuyLink, NULL, NULL, NULL));

	$oProduct->fSearchPrice = 		$oProd->fSearchPrice;
	$oProduct->sProductImageUrl =	$oProd->sAwThumbUrl;
	$oProduct->sProductLink = 		$sProductLink;
	$oProduct->sProductBuyLink = 	$sProductBuyLink;
	$oProduct->sProductName = 		$oProd->sName;

	$aProducts[]= $oProduct;
}

# SMARTY ASSIGN
$oSmarty->assign('aHotPicks', $aProducts);


if ($bAutonomy===true && count($aProducts)>0) {
	$oSmarty->display('elements'.DIRECTORY_SEPARATOR.'hot_picks.tpl');
}



?>
