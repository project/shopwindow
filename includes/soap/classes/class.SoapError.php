<?php

/**
 * A custom error object for handling SoapFaults
 *
 * @copyright	DigitalWindow
 * @author		Kostas Melas <kostas@digitalwindow.com>
 *
 * @package 	AW API
 */
class SoapError
{
	var $sCode;
	var $sString;
	var $sDetails;



	function SoapError($oSoapFault, $sFunctionName)
	{
		$this->sCode= $oSoapFault->faultcode;
		$this->sString= $oSoapFault->faultstring;
		$this->sDetails= $sFunctionName.': '.$oSoapFault->detail->ApiException->message;
	}
}



?>