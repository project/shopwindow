<?php

require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'soap'.DIRECTORY_SEPARATOR.'constants.inc.php');
require_once('class.SoapError.php');


/**
 * PHP5: Extends SoapClient and automatically configures settings specific to the AWin API
 *
 */
class Php5Client extends SoapClient
{
	var $oSoapError= false; // holds any errors produced during the SOAP requests



	/**
	 * The Constructor
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
 	 * @access 	public
 	 *
	 * @param 	object $oUser	the user object with login detail
	 */
	public function __construct($oUser)
	{
		// create client
		#parent::__construct(PS_WSDL, array("trace"=>PS_SOAP_TRACE));
		parent::__construct(PS_WSDL, array('trace'=>PS_SOAP_TRACE, 'compression'=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | SOAP_COMPRESSION_DEFLATE) );
		

		// create headers
		$oHeader= new SoapHeader(PS_NAMESPACE, 'UserAuthentication', $oUser, true, PS_NAMESPACE);
		$oHeader2= new SoapHeader(PS_NAMESPACE, 'getQuota', true, true, PS_NAMESPACE);

		// set headers
		$this->__setSoapHeaders(array($oHeader, $oHeader2));
		
		// set WSDL caching
		ini_set("soap.wsdl_cache_enabled", 1);
		ini_set('soap.wsdl_cache_ttl', 86400);
		
		// set server response timeout
		ini_set('default_socket_timeout', 240);
	}	
	
	
	
	/**
	 * Executes the speficied function from the WSDL
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	public
	 *
	 * @param 	string 	$sFunctionName the name of the function to be executed
	 * @param 	mixed 	$mParams [optional] the parameters to be passed to the function, can be array or single value
	 *
	 * @return 	mixed 	the results or a SoapError object
	 */
	public function call($sFunctionName, $mParams='')
	{
		// catch any exceptions
		try {
			return $this->$sFunctionName($mParams);
		}
		catch(SoapFault $e) {
			$this->oSoapError= new SoapError($e, $sFunctionName);

			return $this->oSoapError;
		}
	}



	/**
	 * Gives the remaining operations quota
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	public
	 *
	 * @return 	int 	the remaining operations quota
	 */
	public function getQuota()
	{
		$sResponse= $this->__getLastResponse();

		// use R.Exp. rather than XML parsers, as they might not be installed
		preg_match('/getQuotaResponse>(.*)<\/.*:getQuotaResponse>/', $sResponse, $aMatches);

		$iQuota= $aMatches[1];

		return $iQuota;
	}





} // end class

?>