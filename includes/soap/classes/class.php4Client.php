<?php

require_once(HOME_PATH.'includes'.DIRECTORY_SEPARATOR.'soap'.DIRECTORY_SEPARATOR.'constants.inc.php');
require_once('class.SoapError.php');

// NuSoap class "soapclient" was renamed to "nusoapclient"
// to avoid conflicts on PHP5 systems with the SOAP ext
require_once('nusoap'.DIRECTORY_SEPARATOR.'nusoap.php');
require_once('nusoap'.DIRECTORY_SEPARATOR.'class.wsdlcache.php');




/**
 * PHP4: Extends SoapClient and automatically configures settings specific to the AWin API
 *
 * NuSoap class "soapclient" was renamed to "nusoapclient"
 * to avoid conflicts on PHP5 systems with the SOAP ext
 *
 */
class Php4Client extends nusoapclient
{
	var $aSoapOptions= array('namespace'=> PS_NAMESPACE, 'trace'=> PS_SOAP_TRACE);
	var $oSoapError= false; // holds any errors produced during the SOAP requests



	/**
	 * The Constructor
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
 	 * @access	public
 	 *
	 * @param 	object $oUser	the user object with login detail
	 *
	 * @return 	object
	 */
	function Php4Client($oUser)
	{
		$oWsdl = false;
		
		// enable WSDL caching
		if (is_writable('/tmp/')) {
			// set lifetime to a day
			$oWsdlCache = new wsdlcache('/tmp/', 86400);		
			$oWsdl = $oWsdlCache->get(PS_WSDL);
			
			// last attempt at caching
			if (empty($oWsdl)) {
				$oWsdl = new wsdl(PS_WSDL);
				$oWsdlCache->put($oWsdl);
			}
		}
		// can't cache, load normally
		else {
			$oWsdl = new wsdl(PS_WSDL);
		}
		
		
		// create client
		parent::nusoapclient($oWsdl, true);

		// set compression for SOAP
		$this->setHTTPEncoding('gzip, deflate');		
		
		// create headers
		$oHeader= new soapval('api:UserAuthentication', false, $oUser);
		$oHeader2= new soapval('api:getQuota', false, true, false, false, array('SOAP-ENV:mustUnderstand'=>1, 'SOAP-ENV:actor' =>PS_NAMESPACE));

		// add headers
		$this->setHeaders(array($oHeader, $oHeader2));
		
		// set server response timeout
		$this->response_timeout = 240;
	}



	/**
	 * Executes the speficied function from the WSDL
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	public
	 *
	 * @param 	string 	$sFunctionName the name of the function to be executed
	 * @param 	mixed 	$params [optional] the parameters to be passed to the function, can be array or single value
	 *
	 * @return 	mixed 	the results or a SoapError object
	 */
	function call($sFunctionName, $params='')
	{
		# Construct the PHP5 conformed XML string to be passed, from the given parameters
		$sParamsXml= $this->_conformParams($params);

		// wrap with functionname
		$sWrapperXml= '<api:'.$sFunctionName.'>'.$sParamsXml.'</api:'.$sFunctionName.'>';


		# make SOAP call
		$response= parent::call($sFunctionName, $sWrapperXml, PS_NAMESPACE, '', false, true);

		// check for SoapFault
		if ( $this->fault===true || empty($response) ) {
			// force a SoapFault on timeouts
			if (empty($response)) {
				$response['faultcode'] = 'HTTP';
				$response['faultstring'] = 'Possible timeout';
				$response['detail']['ApiException']['message'] = 'Nothing was returned, which indicates a possible timeout. Try increasing the variable "$this->response_timeout" within "class.php4Client.php"';
			}
			
			// conform to PHP 5 fault format and pass to SoapError
			$oTempError= new stdClass();
			$oTempError->faultcode= 	$response['faultcode'];
			$oTempError->faultstring= 	$response['faultstring'];
			$oTempError->detail->ApiException->message= $response['detail']['ApiException']['message'];

		    $this->oSoapError= new SoapError($oTempError, $sFunctionName);

			return $this->oSoapError;
		}

		#die(print_r($this));

		$aDataObjs= array();

		// construct the object's return property name
		$sReturnName= $sFunctionName.'Return';

		// NuSoap returns a wrapper array in return, keyed by the data type
		// since we return an single array of objects, the wrapper array should only have a single key
		// so get that and return just the actuall results
		$aReturn= $response[$sReturnName];


		// quick hack for the aKeywordSuggestions array
		foreach ($response as $key=>$val) {
			if (strtolower($key)=='akeywordsuggestions' && is_array($val)) {
				// remove the unnecessary wrapper array
				$response[$key]= $val['item'];
			}
		}


		// return might be empty so check
		if (is_array($aReturn)) {
			$aKeys= array_keys($aReturn);

			// get the actuall resultset
			$aDataArrays= $aReturn[$aKeys[0]];

			// on single object returns, the wrapping array is removed by NuSoap
			// so make sure we put it back into a wrap array
			if (empty($aDataArrays[0])) {
				$aDataArrays= array($aDataArrays);
			}

			// NuSoap returns associative arrays instead of objects
			// so convert them into object for consistency with PHP5
			// (here $aData is usually the actuall data object)
			foreach ($aDataArrays as $aData) {
				$aDataObjs[]= $this->_conformData($aData);
			}
		}
		// always return an array
		else {
			$aDataObjs= array();
		}


		// create the return object
		$oReturn= new stdClass();
		$oReturn->{$sReturnName}= $aDataObjs;

		// assign the rest of the returned variables, if any
		foreach ($response as $key=>$val) {
			// ignoring the already processed return
			if ($key!=$sReturnName) {
				$oReturn->{$key}= $val;
			}
		}


		return $oReturn;
	}



	/**
	 * Converts the parameters passed into PHP5 conformed XML SOAP string
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	private
	 *
	 * @param 	mixed 	$params the parameters to be passed to the function, can be array or single value
	 *
	 * @return 	string 	the resulting XML SOAP parameters string
	 */
	function _conformParams($params)
	{
		$sParamsXml= '';

		// array
		if (is_array($params)) {

			# determine if we have an associative or numeric array
			// numeric
			if ( is_numeric(implode(array_keys($params))) ) {
				// at the moment, all numeric params are integers
				foreach ($params as $iParam) {
					// if val is an array itself, re-occur
					if (is_array($iParam)) {
						$iParam= $this->_conformParams($iParam);
					}

					$sParamsXml.= '<xsd:int>'.$iParam.'</xsd:int>';
				}
			}
			// associative
			else {
				foreach ($params as $sKey=>$sVal) {
					// if val is an array itself, re-occur
					if (is_array($sVal)) {
						$sVal= $this->_conformParams($sVal);
					}

					// force booleans into strings
					if ($sVal===true) {
						$sVal= 'true';
					}
					elseif ($sVal===false) {
						$sVal= 'false';
					}

					$sParamsXml.= '<xsd:'.$sKey.'>'.$sVal.'</xsd:'.$sKey.'>';
				}
			}
		}
		// a single value
		else {
			$sParamsXml= $params;

			// force booleans into strings
			if ($sParamsXml===true) {
				$sParamsXml= 'true';
			}
			elseif ($sParamsXml===false) {
				$sParamsXml= 'false';
			}

		}


		return $sParamsXml;
	}



	/**
	 * Converts arrays(PHP4) into objects(PHP5) recursively, where appropriate
	 *
	 * NuSoap returns objects as associative arrays keyed by the WSDL datatype name.
	 * What we want is to convert those arrays (but only those) to actuall objects and
	 * the rest of the arrays to simple numeric arrays holding those objects.
	 * That creates the problem of not knowing if an array is actually an object or a simple array variable,
	 * so we'll identify arrays by their Hungarian notation naming
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	private
	 *
	 * @param 	array 	$aData the associative array to be converted
	 *
	 * @return 	object 	the resulting data object
	 */
	function _conformData($aData)
	{
		// init
		$oData= new stdClass();

		# Convert arrays into objects as needed, recursively
		# and remove unnecessary nesting
		foreach ($aData as $sPropertyKey=>$propertyVal) {
			unset($mFinalValue);

			// recursive callback
			if (is_array($propertyVal)) {
				$propertyVal= $this->_conformData($propertyVal);
			}

			# for now, the value is unchanged
			$mFinalValue= $propertyVal;


			// if variable name starts with a lowercase "a" followed by an uppercase character
			// that means it is an array (in Hungarian notation)
			if (preg_match('/\ba[A-Z]/', $sPropertyKey)==1 && is_object($propertyVal)) {

				// property should be an array, so get the values to re-assign
				$aPropertyObjVars= get_object_vars($propertyVal);
				$aPropertyObjVarValues= array_values($aPropertyObjVars);

				# overwrite final value
				// here we've converted the container object into an array
				$mFinalValue= $aPropertyObjVarValues;

				// look for deeper container objects
				if (is_object($aPropertyObjVarValues[0])) {

					// get the value of this object's first property
					$aSubPropertyObjVars= get_object_vars($aPropertyObjVarValues[0]);
					$aSubPropertyObjVarValues= array_values($aSubPropertyObjVars);

					// object's first property is itself an object, and is also its sole property
					// which means it's only a container object, and therefore unnecessary
					if ( is_object($aSubPropertyObjVarValues[0]) && count($aPropertyObjVarValues)==1 ) {
						# overwrite final value
						// here we've removed the unnecessary step of the container object
						// and assigned its values up one level, to the containing array
						$mFinalValue= $aSubPropertyObjVarValues;
					}
				}

			}

			# assign final value back to object property
			$oData->{$sPropertyKey}= $mFinalValue;

		} // foreach



		return $oData;
	}



	/**
	 * Gives the remaining operations quota
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	public
	 *
	 * @return 	int 	the remaining operations quota
	 */
	function getQuota()
	{
		$sResponse= $this->__getLastResponse();

		// use R.Exp. rather than XML parsers, as they might not be installed
		preg_match('/getQuotaResponse>(.*)<\/.*:getQuotaResponse>/', $sResponse, $aMatches);

		$iQuota= $aMatches[1];

		return $iQuota;
	}



	/**
	 * Gives the last HTTP request content
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	public
	 *
	 * @return 	string	the contents of the last HTTP request
	 */
	function __getLastRequest()
	{
		# rip the HTTP header
		$sRequest= $this->request;
		$iPos= strpos($sRequest, "<?xml ");

		return substr($sRequest, $iPos);
	}



	/**
	 * Gives the last HTTP request headers content
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	public
	 *
	 * @return 	string	the contents of the last HTTP request headers
	 */
	function __getLastRequestHeaders()
	{
		# stem the HTTP header
		$sHeaders= $this->request;
		$iPos= strpos($sHeaders, "<?xml ");

		return substr($sHeaders, 0, $iPos);
	}



	/**
	 * Gives the last HTTP response content
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	public
	 *
	 * @return 	string	the contents of the last HTTP request headers
	 */
	function __getLastResponse()
	{
		return $this->responseData;
	}



	/**
	 * Gives the last HTTP response headers content
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
	 *
	 * @access 	public
	 *
	 * @return 	string	the contents of the last HTTP request headers
	 */
	function __getLastResponseHeaders()
	{
		# stem the HTTP header
		$sHeaders= $this->response;
		$iPos= strpos($sHeaders, "<?xml ");

		return substr($sHeaders, 0, $iPos);
	}



} // end class

?>