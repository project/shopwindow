<?php

/**
 * Creates and returns the appropriate SOAP client for the current version of PHP
 *
 */
class ClientFactory
{

	/**
	 * Creates and returns the appropriate SOAP client for the current version of PHP
	 *
	 * @copyright	DigitalWindow
 	 * @author		Kostas Melas <kostas@digitalwindow.com>
 	 *
	 * @param int 	 $iUserId
	 * @param string $sUserPassword
	 * @param string $sUserType
	 */
	function getClient($iUserId, $sUserPassword, $sUserType)
	{
		// create user object
		$oUser= new stdClass();
		$oUser->iId= $iUserId;
		$oUser->sPassword= $sUserPassword;
		$oUser->sType= $sUserType;

		// get PHP ver
		$iPhpVer= str_replace('.', '', phpversion());

		// PHP5 (only 5.0.5 onwards has __setSoapHeaders)
		if (class_exists('SoapClient') && $iPhpVer>505 && 1==1) {

			// use PHP5 ext
			require_once('class.php5Client.php');

			$oClient= new Php5Client($oUser);
		}
		// PHP4, try including NuSoap
		elseif (require_once('nusoap'.DIRECTORY_SEPARATOR.'nusoap.php')) {

			// use NuSOAP client
			require_once('class.php4Client.php');

			$oClient= new Php4Client($oUser);
		}
		// no SOAP client could be initialized
		else {
			die('You dont have any SOAP package/ext installed');
		}


		return $oClient;
	}



} // end class




?>