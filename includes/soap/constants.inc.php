<?php

/**
 * Constants used by the API
 *
 */

ini_set("soap.wsdl_cache_enabled", 1);

define('PS_SOAP_TRACE', false);
define('PS_PRINT_SOAP_ERRORS', true);
define('PS_PRINT_SOAP_DEBUG', false);

define('PS_WSDL', 'http://api.productserve.com/v1/ProductServeService?wsdl');
define('PS_NAMESPACE', 'http://api.productserve.com/');


define('PS_USERID', USER_ID);
define('PS_USERPASS', USER_PASS);
define('PS_USERTYPE', 'affiliate');

?>
