; config.inc
; SMARTY SW Template settings
; KM - 12/2006



; [GENERAL SETTINGS]
currencySign	=	"&pound;"
siteTitle		=	"ShopWindow Client"
templateDir		=	"template_2b_green"
categoryTreeCounts	=	"no"



; [CHARACTER LIMITS]
productNameMax		=	"40"
productDescMax		=	"300"
keywordSuggestMax	=	"8"



; [EXTERNAL LINKS]
PopUpWindow		=	"no"
PopUpSettings	=	"width=800,height=600,resizable,location,menubar,scrollbars,status,toolbar"
