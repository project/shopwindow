Shopwindow Client Module For Drupal
Version 0.1
===================================


This is a quick and dirty module that allows you to combine the Shopwindow toolset with the amazing functionality and adapatability a drupal site offers you.

Due to issues with my server I have added caching for everything except product results, so the category tree, hotpicks etc can be loaded as frequently or rarely as you wish, using Drupal's inbuild caching system. I'll add the product caching soon.

Everything is released under GPL - both Affiliate Window's original code and my additions to it.

Installation
============

* Extract the module into your modules directory (usually sites/all/modules )

* Go to the modules page ( admin/build/modules) and enable the module

* Go to the settings page ( admin/settings/shopwinow ) and configure the module. Nothing will work until you at least insert your affiliate id and shopwindow password (you can get this from AW's 'Update Account' page)

* Go to the blocks page and enable the blocks you want

Seeing your products
====================

You can navigate to browse/(categoryid) or browse/0/(search) to see products matching that category or search, or browse/(categoryid)/(search) to see all (search) products in (categoryid)

You can create custom blocks with links to certain searches if you wish, or set the homepage to the category you want as I have done in http://www.coffeemakers.org.uk , enabling you to create a quick and easy niche site.

However I'd suggest to get the most out of this module that you start creating product nodes which are far more integrated with Drupal's content handling systems


Creating Product Nodes
======================

Optionally (but highly recommended) : Install CCK and Computed Fields modules, as well as content_copy. Load the file content_types_product.txt from this directory in Notepad and paste into the Import Content page (admin/content/types/import)

Now you can create 'product' nodes ('node/add/product') where the title of the node is the search terms passed to ShopWindow, allowing you to write blog posts, features or reviews of products and have their current prices displayed quickly and easily, making it simple to create content-rich niche affiliate sites. Of course you can write what you want for the product description in the 'Body' field.

If you find the results displayed are not accurate enough refine your title or enter the category id you wish to search from in the 'cid' box on the add/edit node page.

(hint: install the FiveStar module and add the fivestar field type to your product node for instant 'product review' nodes)

Can I use node_import to create millions of product nodes?
==========================================================

This is where things get even more fun - you can create big lists of product titles, (and categories if you wish) run them through node_import, and instantly create a shopping site listing all the products you have given it.

The only problem with this approach is for some reason I haven't pinned down the $_POST value gets reset when creating product nodes, which node_import relies on to decide where you are in the import process.

IF you wish to use node import you will need to open node_import.module, navigate to line 66:
  if ($_SESSION['node_import_page'] && $_POST) {
and replace it with this:
  if ($_SESSION['node_import_page']) {

NOTE: This will cause strange behaviour, but it will allow you to import the nodes you want. Do so at your own risk, change it back when you're done and I take no responsibility for harm incurred :)
Also : If you import millions of nodes it is likely PHP will time out. Either do it in batches or increase your max_execution_time server setting.


Outstanding issues
==================

* I don't think the module should continue to use the Smarty templates provided but should follow Drupal's theming conventions allowing full css control - this will stop the css etc disrupting existing designs. Infact I should break away from the client software entirely and just use the API

* I haven't fixed up the searchbox block properly, and have used a custom block on my own sites

* There is no (category)+(merchantid) search

* I haven't implemented caching on product searches

