{include file="elements`$DIRECTORY_SEPARATOR`refineby.tpl"}
<div class="sw_productlist">

<ul>

{foreach from=$aProducts item=oProduct}
	<li class="sw_productentry">
	<div class="sw_productphoto">
	<a href="{$oProduct->sProductLink}"><img src="{$oProduct->sProductImageUrl|default:$noImage}" /></a></div>

	<div class="sw_productdesc">
	<h3><a href="{$oProduct->sProductLink}">{$oProduct->sProductName|truncate:#productNameMax#}</a> from {$oProduct->sMerchantName}</h3>

	<p>{$oProduct->sProductPromo|truncate:#productDescMax#}</p>
	<p>{$oProduct->sProductDescription|truncate:#productDescMax#}</p>

	<span class="sw_productcat">Category: <a href="{$oProduct->sCategoryLink}">{$oProduct->sCategoryName}</a></span>

	<span class="sw_productbuy">
		{if #PopUpWindow#=='yes'}
			<a href="#" onclick="window.open('{$oProduct->sProductBuyLink}', '', '{#PopUpSettings#}')">
		{else}
			<a href="{$oProduct->sProductBuyLink}">
		{/if}
		{#currencySign#}{$oProduct->fSearchPrice|number_format:2} <img src="{$templatePath}/images/buy_button.gif" title="Buy Now!" /> </a>
	</span>
	<div>&nbsp;</div>
	</div>



	<div class="sw_productdivider">&nbsp;</div>

	</li>
{/foreach}

</ul>
<div class="sw_productdivider">&nbsp;</div>
</div>
