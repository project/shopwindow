<div class="sw_productlist">

<a href="{$sProductLink}">
	<img class="sw_productlogo" src="{$sMerchantLogoUrl|default:$noLogo}" border="0" />
 	<img class="sw_productphoto" src="{$sProductImageUrl|default:$noImage}">
</a>

<h3>{$sProductName} from <a href="{$sMerchantLink}">{$sMerchantName}</a></h3>
<p>{$sProductDesc}</p>
<p>{$sProductPromo}</p>

<span class="sw_productcat">Category: <a href="{$sProductCategoryLink}">{$sProductCategoryName}</a> </span>

<span class="sw_productbuy">
	 {#currencySign#}{$fSearchPrice|number_format:2}<br /><a href="{$sProductLink}">Buy Now!</a>
</span>

</div>

<div style="clear: both; height: 1px; border: 0;">&nbsp;</div>