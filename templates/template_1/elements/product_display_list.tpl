{include file="elements`$DIRECTORY_SEPARATOR`refineby.tpl"}
<div class="sw_productlist">
<ul>

{foreach from=$aProducts item=oProduct}
	<li class="sw_productentry">
	<a href="{$oProduct->sProductLink}"><img class="sw_productphoto" src="{$oProduct->sProductImageUrl|default:$noImage}" /></a>

	<div class="sw_productdesc">
	<h3><a href="{$oProduct->sProductLink}">{$oProduct->sProductName|truncate:#productNameMax#}</a> from {$oProduct->sMerchantName}</h3>

	<p>{$oProduct->sProductPromo|truncate:#productDescMax#}</p>
	<p>{$oProduct->sProductDescription|truncate:#productDescMax#}</p>

	<span class="sw_productcat">Category: <a href="{$oProduct->sCategoryLink}">{$oProduct->sCategoryName}</a></span>

	<span class="sw_productbuy">
		 {#currencySign#}{$oProduct->fSearchPrice|number_format:2}<br /><a href="{$oProduct->sProductBuyLink}">Buy Now!</a>
	</span>

	</div>
	

	<div class="sw_productdivider">&nbsp;</div>

	</li>
{/foreach}

</ul>
<div class="sw_productdivider">&nbsp;</div>
</div>
