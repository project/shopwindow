
<div class="sw_hotpicks">
<h2>Hot Picks</h2>

<ul>

{if count($aHotPicks)>0}

	{foreach from=$aHotPicks item=oProduct}

		<li>
			<span><a href="{$oProduct->sProductLink}"><img src="{$oProduct->sProductImageUrl|default:$noImage}" /></a></span>
 
			<a href="{$oProduct->sProductLink}"><h3>{$oProduct->sProductName|truncate:#productNameMax#}</h3></a>


			<a href="{$oProduct->sProductBuyLink}"> {#currencySign#}{$oProduct->fSearchPrice|number_format:2} </a>
		</li>

	{/foreach}

{/if}

</ul>
<div></div>

</div>
