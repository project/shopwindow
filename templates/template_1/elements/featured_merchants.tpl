
{* Only show if there are any *}

{if count($aFeaturedMerchants)>0 }

	<div class="sw_featuredmerch">
	<h3>Featured Retailers</h3>
	<ul>

    {foreach from=$aFeaturedMerchants item=oFeaturedMerchant name=FeaturedMerchants}

    		<li>

		<a href="{$oFeaturedMerchant->sCategoryMerchantLink}"><img src="{$oFeaturedMerchant->sLogoUrl|default:$noLogo}" border="0"/></a>

		<div id="sw_featuredtext">
			<a href="{$oFeaturedMerchant->sCategoryMerchantLink}"><h4>{$oFeaturedMerchant->sName}</h4></a>
			<a href="{$oFeaturedMerchant->sCategoryMerchantLink}">View products</a> <br/>
			<a href="{$oFeaturedMerchant->sLink}">View site</a> <br/>
		</div>

		</li>

	{/foreach}

    </ul>

    <div class="sw_catsdivider"></div>

    </div>

{/if}