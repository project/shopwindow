<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>{#siteTitle#} - Where do you want to shop today?</title>
<link rel="stylesheet" href="{$templatePath}/css/styles.css" type="text/css">
</head>

<body>

<div id="sw_frame">

	{include file="elements`$DIRECTORY_SEPARATOR`page_header.tpl"}

	<div id="sw_nav">
		{include file="elements`$DIRECTORY_SEPARATOR`category_tree.tpl"}
	</div>

	<div id="sw_container">
		<div id="sw_content">
		{include file="elements`$DIRECTORY_SEPARATOR`search_box.tpl"}
		{include file="elements`$DIRECTORY_SEPARATOR`hot_picks.tpl"}
		</div>
	</div>

{include file="elements`$DIRECTORY_SEPARATOR`page_footer.tpl"}