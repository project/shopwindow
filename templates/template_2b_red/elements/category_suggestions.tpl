

{if count($aCategorySuggestions)>0}

<div class="sw_searchsuggestion">
	
	<h3>Suggested Categories</h3>
	<ul>

	{foreach from=$aCategorySuggestions item=oCatWord name=catkeyword}
		    <li><a href="{$oCatWord->sCategoryLink}">{$oCatWord->sName}</a></li>
	{/foreach}

	</ul>
</div>

{/if}

