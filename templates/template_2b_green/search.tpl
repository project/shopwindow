<link rel="stylesheet" href="{$templatePath}/css/styles.css" type="text/css">
<div id="sw_frame">
<div id="sw_container">
<div id="sw_content">
	{include file="elements`$DIRECTORY_SEPARATOR`hot_picks.tpl"}
{if $iProducts>0}

		{include file="elements`$DIRECTORY_SEPARATOR`nav_product_list_top.tpl"}

		<br /><br />

		{include file="elements`$DIRECTORY_SEPARATOR`product_display_list.tpl"}

		{include file="elements`$DIRECTORY_SEPARATOR`nav_product_list_bottom.tpl"}

{else}
	<div class="sw_error">
		<h3>No Results Found</h3>
		<p>We could not find anything to match your search. Please refine your search terms.</p>
	</div>

{/if}


</div>
</div>

{include file="elements`$DIRECTORY_SEPARATOR`page_footer.tpl"}