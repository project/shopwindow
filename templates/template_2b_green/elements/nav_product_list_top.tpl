
<form class="" action="{$PRODUCTLIST_PAGE}" method="GET">

	<div id="sw_resultsheader">
		<div id="sw_pagination">

			{if !empty($sPreviousLink)}
				<a href="{$sPreviousLink}" style="float:left;">&lt;&lt; Previous Page</a>
			{/if}

			{if !empty($sNextLink)}
				<a href="{$sNextLink}" style="float:right;">Next Page &gt;&gt;</a>
			{/if}

			{$iListOffset+1|number_format} - {$iPageProducts|number_format} of {$iTotalProducts|number_format}

		</div>

	</div>
</form>