<div class="sw_productlist">

{if #PopUpWindow#=='yes'}
	<a href="#" onclick="window.open('{$sProductLink}', '', '{#PopUpSettings#}')">
{else}
	<a href="{$sProductLink}">
{/if}
	<img class="sw_productlogo" src="{$sMerchantLogoUrl|default:$noLogo}" border="0" />
 	<img class="sw_productphoto" src="{$sProductImageUrl|default:$noImage}">
</a>

<h3>{$sProductName} from
{if #PopUpWindow#=='yes'}
		<a href="#" onclick="window.open('{$sMerchantLink}', '', '{#PopUpSettings#}')">
	{else}
		<a href="{$sMerchantLink}">
	{/if}
{$sMerchantName}</a></h3>
<p>{$sProductDesc}</p>
<p>{$sProductPromo}</p>

<span class="sw_productcat">Category: <a href="{$sProductCategoryLink}">{$sProductCategoryName}</a> </span>

<span class="sw_productbuy">
	{if #PopUpWindow#=='yes'}
		<a href="#" onclick="window.open('{$sProductLink}', '', '{#PopUpSettings#}')">
	{else}
		<a href="{$sProductLink}">
	{/if}
	{#currencySign#}{$fSearchPrice|number_format:2} <img src="{$templatePath}/images/buy_button.gif" title="Buy Now!" /> </a>
</span>

</div>

<div style="clear: both; height: 1px; border: 0;">&nbsp;</div>