{include file="elements`$DIRECTORY_SEPARATOR`refineby.tpl"}
<div>

<table>

{foreach from=$aProducts item=oProduct}
	{cycle values='<tr><td class="sw_productlisttop">, <td class="sw_productlisttop">'}
	<table width="100%" height="100%" cellpadding="0" cellspacing="0"><tr><td class="sw_productlistmain" colspan="2">
	<div class="sw_productphoto">
	<a href="{$oProduct->sProductLink}"><img src="{$oProduct->sProductImageUrl|default:$noImage}" /></a></div>
	

	<div class="sw_productdesc">
	<h3><a href="{$oProduct->sProductLink}">{$oProduct->sProductName|truncate:#productNameMax#}</a> from {$oProduct->sMerchantName}</h3>
	<span class="sw_productbuy">
		{if #PopUpWindow#=='yes'}
			<a href="#" onclick="window.open('{$oProduct->sProductBuyLink}', '', '{#PopUpSettings#}')">
		{else}
			<a href="{$oProduct->sProductBuyLink}">
		{/if}
		{#currencySign#}{$oProduct->fSearchPrice|number_format:2} <img src="{$templatePath}/images/buy_button.gif" title="Buy Now!" /> </a>
	</span>
	

	<span class="sw_productcat">Category: <a href="{$oProduct->sCategoryLink}">{$oProduct->sCategoryName}</a></span>

	<p>{$oProduct->sProductPromo|strip_tags:true|truncate:#productDescMax#}</p>
	<p>{$oProduct->sProductDescription|strip_tags:true|truncate:#productDescMax#}</p>

	</div>
	</td></tr>

	<tr><td class="sw_productlistbottomleft">&nbsp;</td><td class="sw_productlistbottomright">&nbsp;</td></tr>
	</table>
	
	
	</td>
{/foreach}

</table>
<div class="sw_spacer">&nbsp;</div>
</div>
