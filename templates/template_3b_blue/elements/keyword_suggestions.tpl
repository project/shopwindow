{if count($aKeywordSuggestions)>0}

<div class="sw_searchsuggestion">

	<h3>{$sSearchTerm}?</h3><p>&nbsp; Did you mean:

	{foreach from=$aKeywordSuggestions item=oKeyword name=keyword}
		{if $smarty.foreach.keyword.iteration<=#keywordSuggestMax#}
		    <a href="{$oKeyword->sSearchLink}">{$oKeyword->sKeyword}</a>{if $smarty.foreach.keyword.iteration < #keywordSuggestMax#},{/if}&nbsp;
		{/if}
	{/foreach}
</p>
</div>

{/if}