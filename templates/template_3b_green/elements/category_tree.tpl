
{if count($aCategories)>0}


	<div class="sw_boxheader"><h2>{$sTreeTitle} {$sCatExplained}</h2></div>

	<div class="sw_cats">



	<ul>


	{foreach from=$aCategories item=oCategory}

		<li>


		<h3><a href="{$oCategory->sLink}">{$oCategory->sName}
	    {if $oCategory->iTotalCount!=''}
	    	({$oCategory->iTotalCount|number_format})
	    {/if}
	    </a>
	    </h3>


		{* Display three random grand children categories, when browsing front page Super Categories *}

		{if $oCategory->iParentId==0}
			{foreach from=$oCategory->aThreeRandomGrandChildren item=oGrandChild name=aThreeRandomGrandChildren}
				<a href="{$oGrandChild->sLink}">{$oGrandChild->sName}</a>
				{if $oGrandChild->iTotalCount!=''}
					({$oGrandChild->iTotalCount|number_format})
				{/if}
				{if $smarty.foreach.aThreeRandomGrandChildren.last!=true}, {/if}
			{/foreach}
		{/if}


		{* Display Children - when now browsing super cats *}

		{if $oCategory->iParentId>0 && is_array($oCategory->aChildren)}
			{foreach from=$oCategory->aChildren item=oChildCategory}
				<a href="{$oChildCategory->sLink}">{$oChildCategory->sName} </a>
				{if $oChildCategory->iTotalCount!=''}
					({$oChildCategory->iTotalCount|number_format})
				{/if}
				<br />
			{/foreach}
		{/if}


		</li>

	{/foreach}


	</ul>

	<div class="sw_productdivider"></div>
	</div>

{/if}