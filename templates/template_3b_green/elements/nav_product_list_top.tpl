
<form class="" action="{$PRODUCTLIST_PAGE}" method="GET">
    {if strlen($iCategoryId)>0} <input type="hidden" name="c" value="{$iCategoryId}" />	{/if}
    {if strlen($iMerchantId)>0} <input type="hidden" name="m" value="{$iMerchantId}" />	{/if}
    {if strlen($sQuery)>0} 		<input type="hidden" name="q" value="{$sQuery}" />		{/if}

	<div id="sw_resultsheader">
		<span id="sw_prodsperpage">
			Products per page:

			{html_options name="iListLimit" options=$aSelectLimits selected=$iListLimit onChange="submit();"}

		</span>

		<span id="sw_sortby">
			Sort by:

			{html_options name="sListSort" options=$aSelectSorts selected=$sListSort onChange="submit();"}

		</span>

		<div id="sw_pagination">

			{if !empty($sPreviousLink)}
				<a href="{$sPreviousLink}" style="float:left;">&lt;&lt; Previous Page</a>
			{/if}

			{if !empty($sNextLink)}
				<a href="{$sNextLink}" style="float:right;">Next Page &gt;&gt;</a>
			{/if}

			{$iListOffset+1|number_format} - {$iPageProducts|number_format} of {$iTotalProducts|number_format}

		</div>

	</div>
</form>