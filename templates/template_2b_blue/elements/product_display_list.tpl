{include file="elements`$DIRECTORY_SEPARATOR`refineby.tpl"}
<div class="sw_productlist">

<table>

{foreach from=$aProducts item=oProduct}

	{cycle values='<tr><td class="sw_productentry">, <td class="sw_productentry">'}
	<div class="sw_productphoto">
	<a href="{$oProduct->sProductLink}"> <img src="{$oProduct->sProductImageUrl|default:$noImage}" width="90" height="90"> </a></div>


	<div class="sw_productdesc">
	<h3><a href="{$oProduct->sProductLink}">{$oProduct->sProductName|truncate:#productNameMax#}</a> from {$oProduct->sMerchantName}</h3>
	<span class="sw_productbuy">
		{if #PopUpWindow#=='yes'}
			<a href="#" onclick="window.open('{$oProduct->sProductBuyLink}', '', '{#PopUpSettings#}')">
		{else}
			<a href="{$oProduct->sProductBuyLink}">
		{/if}
		{#currencySign#}{$oProduct->fSearchPrice|number_format:2} <img src="{$templatePath}/images/buy_button.gif" title="Buy Now!" /> </a>
	</span>


	<span class="sw_productcat">Category: <a href="{$oProduct->sCategoryLink}">{$oProduct->sCategoryName}</a></span>

	<p>{$oProduct->sProductPromo|strip_tags:true|truncate:#productDescMax#}</p>
	<p>{$oProduct->sProductDescription|strip_tags:true|truncate:#productDescMax#}</p>

	</div>


	</td>
{/foreach}

</tr>
</table>
<div class="sw_spacer">&nbsp;</div>
</div>