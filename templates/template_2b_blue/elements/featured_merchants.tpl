
{* Only show if there are any *}

{if count($aFeaturedMerchants)>0 }

	<div class="sw_featuredmerch">
	<h3>Featured Retailers</h3>
	<ul>

    {foreach from=$aFeaturedMerchants item=oFeaturedMerchant name=FeaturedMerchants}

    		<li>

		<a href="{$oFeaturedMerchant->sLink}"><img src="{$oFeaturedMerchant->sLogoUrl|default:$noLogo}" border="0"/></a>

		<div id="sw_featuredtext">
			<a href="{$oFeaturedMerchant->sLink}"><h4>{$oFeaturedMerchant->sName}</h4></a>
			{if #PopUpWindow#=='yes'}
				<a href="#" onclick="window.open('{$oFeaturedMerchant->sLink}', '', '{#PopUpSettings#}')">View site</a> <br/>
			{else}
				<a href="{$oFeaturedMerchant->sLink}">View site</a> <br/>
			{/if}
		</div>

		</li>

	{/foreach}

    </ul>

    <div class="sw_catsdivider"></div>

    </div>

{/if}