<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<title>{#siteTitle#} {$sBreadCrumbText}</title>
<link rel="stylesheet" href="{$templatePath}/css/styles.css" type="text/css">
</head>

<body>

<div id="sw_frame">

{include file="elements`$DIRECTORY_SEPARATOR`page_header.tpl"}

<div id="sw_nav">
{include file="elements`$DIRECTORY_SEPARATOR`category_tree.tpl"}
{include file="elements`$DIRECTORY_SEPARATOR`featured_merchants.tpl"}
</div>


<div id="sw_container">
<div id="sw_content">
{include file="elements`$DIRECTORY_SEPARATOR`search_box.tpl"}
<div id="sw_breadcrumb"> {$sBreadCrumbHtml} </div>

{if $iProducts>0}
	{include file="elements`$DIRECTORY_SEPARATOR`nav_product_list_top.tpl"}
	{include file="elements`$DIRECTORY_SEPARATOR`product_display_list.tpl"}
	{include file="elements`$DIRECTORY_SEPARATOR`nav_product_list_bottom.tpl"}
{/if}

</div>
</div>
{include file="elements`$DIRECTORY_SEPARATOR`page_footer.tpl"}