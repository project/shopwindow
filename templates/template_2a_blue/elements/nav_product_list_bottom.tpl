
<div id="sw_pagination">

	{if !empty($sPreviousLink)}
		<a href="{$sPreviousLink}" style="float:left;">&lt;&lt; Previous Page</a>
	{/if}

	{if !empty($sNextLink)}
		<a href="{$sNextLink}" style="float:right;">Next Page &gt;&gt;</a>
	{/if}

	{if count($aPagenator)>0}
		Pages: &nbsp; [
		{foreach from=$aPagenator key=iKey item=sPageLink name=aPagenator}
			{if $iCurrentPage==$iKey}
				<a href="{$sPageLink}"> <span style="font-weight:bold;">{$iKey|number_format}</span> </a>
			{else}
				<a href="{$sPageLink}"> <span style="font-weight:normal;">{$iKey|number_format}</span> </a>
			{/if}
			{if $smarty.foreach.aPagenator.first || $smarty.foreach.aPagenator.index==($smarty.foreach.aPagenator.total-2)}
				...
			{elseif $smarty.foreach.aPagenator.last!=true}
				 -
			{/if}
		{/foreach}
		]
	{/if}

	<br /><br />


</div>