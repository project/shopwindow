
{* Only show if there are any *}

{if count($aRefineByDefs)>0 }

	<div class="refineby">
	<h3>Refine by</h3>


	<table>

	<tr>
	    {foreach from=$aRefineByDefs item=oRefineByDef}
			<th>{$oRefineByDef->sName}</th>
		{/foreach}
	</tr>

	<tr>
		{foreach from=$aRefineByDefs item=oRefineByDef}
			<td>

			(<a id="clear" href="{$oRefineByDef->sCancelRefineByLink}">clear filter</a>) <br /><br />

			{foreach from=$oRefineByDef->aRefineByValues item=oRefByValue}

				{* Highlight active values *}

				{if in_array($oRefByValue->iId, $aRefineByActiveValueIds)}
					<a href="{$oRefByValue->sRefineByLink}"><strong>{$oRefByValue->sName}</strong></a> <br />
				{else}
					<a href="{$oRefByValue->sRefineByLink}">{$oRefByValue->sName}</a> <br />
				{/if}


			{/foreach}

			</td>
		{/foreach}
	</tr>


	</table>


    </div>

{/if}