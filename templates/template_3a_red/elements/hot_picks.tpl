<div class="sw_boxheader"><h2>Hot Picks</h2></div>
<div class="sw_hotpicks">


<ul>

{if count($aHotPicks)>0}

	{foreach from=$aHotPicks item=oProduct}

		<li>
			<span><a href="{$oProduct->sProductLink}"><img src="{$oProduct->sProductImageUrl|default:$noImage}" /></a></span>
 
			<a href="{$oProduct->sProductLink}"><h3>{$oProduct->sProductName|truncate:#productNameMax#}</h3></a>

			{if #PopUpWindow#=='yes'}
				<a href="#" onclick="window.open('{$oProduct->sProductBuyLink}', '', '{#PopUpSettings#}')">
			{else}
				<a href="{$oProduct->sProductBuyLink}">
			{/if} {#currencySign#}{$oProduct->fSearchPrice|number_format:2} </a>
		</li>

	{/foreach}

{/if}

</ul>
<div class="sw_productdivider"></div>

</div>
